import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import javax.management.MBeanRegistration;

public class Boosts {

    private int x;
    private int y;
    private BoostType boostType;
    private Boosts boost;

    private Rectangle boostOnGrid;

    public Boosts() {

        //this.boostType = boostRand();
        this.boostType = BoostType.APPLE;

        x = posRandX();
        y = posRandY();

        boostOnGrid = new Rectangle(x,y,Grid.CELL_SIZE,Grid.CELL_SIZE);
        switch (boostType){
            case APPLE:
                boostOnGrid.setColor(Color.RED);
                break;
            case SHRINK_MUSHROOMS:
                boostOnGrid.setColor(Color.ORANGE);
                break;
            case SPEED_BEANS:
                boostOnGrid.setColor(Color.CYAN);
                break;
        }

        boostOnGrid.fill();
    }

    public Rectangle getBoostOnGrid() {
        return boostOnGrid;
    }

    public void action(Snake snake) {

        switch (boostType){
            case APPLE:
                snake.creatBodyPart();
                break;

            case SPEED_BEANS:
                if(Game.speed<20){return;}
                Game.speed-=20;
                break;

            case SHRINK_MUSHROOMS:

                break;

        }


    }

    private int posRandX() {
        int x= (int) (Math.random() * Grid.maxCols);
        return x * Grid.CELL_SIZE + Grid.PADDING;
    }

    private int posRandY() {
        // (int) (Math.random() * (Grid.maxRows - Grid.PADDING) + Grid.PADDING);
        int y = (int) (Math.random() * Grid.maxRows);
        return y * Grid.CELL_SIZE + Grid.PADDING;
    }

    public BoostType boostRand(){
        return BoostType.values()[(int) (Math.random() * BoostType.values().length)];
    }

}
