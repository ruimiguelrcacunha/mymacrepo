import org.academiadecodigo.simplegraphics.graphics.Rectangle;


public class Grid {

    public final static int maxCols = 40;
    public final static int maxRows = 50;
    public final static int CELL_SIZE = 15;
    public final static int PADDING = 10;

    private Boosts boost;

    private Rectangle[][] grid = new Rectangle[maxCols][maxRows];

    public Grid() {

        gridInit();
        createBoost();


    }

    public void gridInit() {

        for (int row = 0; row < maxRows; row++) {
            for (int col = 0; col < maxCols; col++) {
                grid[col][row] = new Rectangle(col2x(col) + PADDING, row2y(row) + PADDING, CELL_SIZE, CELL_SIZE);
                grid[col][row].fill();
            }
        }
    }

    private int row2y(int row) {
        return row * CELL_SIZE;
    }

    private int col2x(int col) {
        return col * CELL_SIZE;
    }

    public int getWidth() {
        return col2x(maxCols);
    }

    public int getHeigth() {
        return row2y(maxRows);
    }


    public void checkBoostAndHead(Snake snake) {
        if (snake.getHead().getY() == boost.getBoostOnGrid().getY() && snake.getHead().getX() == boost.getBoostOnGrid().getX()) {
            boost.action(snake);
            boost.getBoostOnGrid().delete();
            createBoost();
            snake.incrementScore();
        }

    }

    public boolean check4Collisions(Snake snake) {
        //border collisions
        if (snake.getHead().getY() > getWidth() + PADDING || snake.getHead().getY() < 0 || snake.getHead().getX() < 0 || snake.getHead().getX() > getHeigth()) {
            return true;
        }
        return snake.check4SelfCollisions();
    }

    public void createBoost() {
        boost = new Boosts();
    }


}
