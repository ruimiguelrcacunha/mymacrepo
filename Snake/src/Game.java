import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;

public class Game implements KeyboardHandler {

    private Grid grid;
    private Snake snake;
    private boolean gameOver = false;

    public static int speed = 200;

    private Keyboard keyboard = new Keyboard(this);

    private KeyboardEvent upPressed = new KeyboardEvent();
    private KeyboardEvent downPressed = new KeyboardEvent();
    private KeyboardEvent rightPressed = new KeyboardEvent();
    private KeyboardEvent leftPressed = new KeyboardEvent();


    public Game() throws InterruptedException {

        grid = new Grid();
        snake = new Snake();

        keyboardInit();
        gameStart();
    }

    private void gameStart() throws InterruptedException {

        while (!gameOver) {

            snake.turn();
            Thread.sleep(speed);

            grid.checkBoostAndHead(snake);
            gameOver = grid.check4Collisions(snake);

        }
        System.out.println("gameover , your score: " + snake.getScore());
    }


    private void keyboardInit() {


        upPressed.setKey(KeyboardEvent.KEY_UP);
        upPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        downPressed.setKey(KeyboardEvent.KEY_DOWN);
        downPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        rightPressed.setKey(KeyboardEvent.KEY_RIGHT);
        rightPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        leftPressed.setKey(KeyboardEvent.KEY_LEFT);
        leftPressed.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);


        keyboard.addEventListener(upPressed);
        keyboard.addEventListener(downPressed);

        keyboard.addEventListener(rightPressed);
        keyboard.addEventListener(leftPressed);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {

        switch (keyboardEvent.getKey()) {
            case KeyboardEvent.KEY_UP:
                if (snake.getDirection() == HeadDirection.DOWN) {
                    return;
                }
                snake.setDirection(HeadDirection.UP);
                break;

            case KeyboardEvent.KEY_DOWN:
                if (snake.getDirection() == HeadDirection.UP) {
                    return;
                }
                snake.setDirection(HeadDirection.DOWN);
                break;

            case KeyboardEvent.KEY_LEFT:
                if (snake.getDirection() == HeadDirection.RIGHT) {
                    return;
                }
                snake.setDirection(HeadDirection.LEFT);
                break;

            case KeyboardEvent.KEY_RIGHT:
                if (snake.getDirection() == HeadDirection.LEFT) {
                    return;
                }
                snake.setDirection(HeadDirection.RIGHT);
                break;


        }


    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {

    }
}
