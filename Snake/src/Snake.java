
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

import java.util.ArrayList;


public class Snake {

    private Rectangle head;
    private ArrayList<SnakeBody> snakeBodies = new ArrayList<>();
    private int numberOfBodyParts = 0;
    private HeadDirection direction;

    private int score = 0;

    public Snake() {
        direction = HeadDirection.UP;
        snakeInit();
    }

    private void snakeInit() {

        head = new Rectangle(col2x(Grid.maxCols / 2) + Grid.PADDING, row2y(Grid.maxRows / 2) + Grid.PADDING, Grid.CELL_SIZE, Grid.CELL_SIZE);
        head.setColor(Color.GREEN);
        head.fill();

    }

    public HeadDirection getDirection() {
        return direction;
    }

    public Rectangle getHead() {
        return head;
    }

    private int row2y(int row) {
        return row * Grid.CELL_SIZE;
    }

    private int col2x(int col) {
        return col * Grid.CELL_SIZE;
    }

    public void creatBodyPart() {
        if(snakeBodies.isEmpty()){
        snakeBodies.add(new SnakeBody(head.getY(), head.getX(), ++numberOfBodyParts));
        return;
        }
        snakeBodies.add(new SnakeBody(snakeBodies.get(numberOfBodyParts-1).getY(), snakeBodies.get(numberOfBodyParts-1).getX(),++numberOfBodyParts));
    }

    public void setDirection(HeadDirection direction) {
        this.direction = direction;
    }

    public void turn() { // involuntary movement
        switch (direction) {
            case UP:
                moveUp();
                break;
            case DOWN:
                moveDown();
                break;
            case LEFT:
                moveLeft();
                break;
            case RIGHT:
                moveRight();
                break;
        }
    }

    private void moveLeft() {
        direction = HeadDirection.LEFT;
        followUp();
        head.translate(-Grid.CELL_SIZE, 0);
    }

    private void moveRight() {
        direction = HeadDirection.RIGHT;
        followUp();
        head.translate(Grid.CELL_SIZE, 0);
    }

    private void moveDown() {
        direction = HeadDirection.DOWN;
        followUp();
        head.translate(0, Grid.CELL_SIZE);
    }

    private void moveUp() {
        direction = HeadDirection.UP;
        followUp();
        head.translate(0, -Grid.CELL_SIZE);
    }


    private void followUp() {
        if (snakeBodies.isEmpty() == true) {
            return;
        }

        if (snakeBodies.size() > 2) {
            for (int body = snakeBodies.size() - 1; body >= 1; body--) {
                int previousX = snakeBodies.get(body - 1).getX();
                int previousY = snakeBodies.get(body - 1).getY();

                snakeBodies.get(body).translate(previousX, previousY);
            }
        }

        int previousX = head.getX();
        int previousY = head.getY();
        snakeBodies.get(0).translate(previousX, previousY);
    }

    public void shrink() {

        snakeBodies.remove(snakeBodies.size());
    }


    public boolean check4SelfCollisions() {
        for (SnakeBody body : snakeBodies) {
            if (body.getX() == head.getX() && body.getY() == head.getY()) {
                return true;
            }
        }
        return false;

    }

    public void incrementScore() {
        score++;
    }

    public int getScore() {
        return score;
    }

    // body class
    private class SnakeBody {

        private Rectangle body;
        private int bodyNumber;

        public SnakeBody(int col, int row, int bodyNumber) {

            this.bodyNumber = bodyNumber;
            snakeBodyInit(col2x(col), row2y(row));

        }

        private void snakeBodyInit(int col, int row) {
            body = new Rectangle(col + Grid.PADDING, row + Grid.PADDING, Grid.CELL_SIZE, Grid.CELL_SIZE);
            body.setColor(Color.DARK_GRAY);
            body.fill();
        }

        public int getX() {
            return body.getX();
        }

        public int getY() {
            return body.getY();
        }

        public void translate(int x, int y) {
            body.delete();
            body = new Rectangle(x, y, Grid.CELL_SIZE, Grid.CELL_SIZE);
            body.setColor(Color.DARK_GRAY);
            body.fill();
        }
    }
}

