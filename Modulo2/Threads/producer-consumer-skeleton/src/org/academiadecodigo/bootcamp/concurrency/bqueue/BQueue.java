package org.academiadecodigo.bootcamp.concurrency.bqueue;

import java.util.LinkedList;
import java.util.List;

/**
 * Blocking Queue
 *
 * @param <T> the type of elements stored by this queue
 */
public class BQueue<T> {


    private LinkedList<T> list;
    private int limit;


    /**
     * Constructs a new queue with a maximum size
     *
     * @param limit the queue size
     */
    public BQueue(int limit) {

        list = new LinkedList<>();
        this.limit = limit;

    }

    /**
     * Inserts the specified element into the queue
     * Blocking operation if the queue is full
     *
     * @param data the data to add to the queue
     */
    public void offer(T data) {
        synchronized (this) {
            while (list.size() == limit) {

                try {
                    System.out.println("##### -> QUEUE IS FULL\n");
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            list.add(data);
            System.out.println(Thread.currentThread().getName() + " : produced");
            System.out.println("#####" + Thread.currentThread().getName() + " -> elements in list: " + getSize() + "\n");
            notifyAll();
        }
    }

    /**
     * Retrieves and removes data from the head of the queue
     * Blocking operation if the queue is empty
     *
     * @return the data from the head of the queue
     */
    public T poll() {
        synchronized (this) {

            while (list.size() == 0) {

                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            notifyAll();
            return list.remove();

        }
    }

    /**
     * Gets the number of elements in the queue
     *
     * @return the number of elements
     */
    public int getSize() {
        synchronized (this) {
            return list.size();
        }
    }

    /**
     * Gets the maximum number of elements that can be present in the queue
     *
     * @return the maximum number of elements
     */
    public int getLimit() {
        synchronized (this) {
            return limit;
        }
    }


}


