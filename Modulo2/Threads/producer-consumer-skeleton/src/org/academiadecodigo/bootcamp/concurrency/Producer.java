package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Produces and stores integers into a blocking queue
 */
public class Producer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;

    /**
     * @param queue      the blocking queue to add elements to
     * @param elementNum the number of elements to produce
     */
    public Producer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {

        while (elementNum > 0) {
            queue.offer(1);
            --elementNum;
        }


            /*synchronized (queue) {

                if (!isQueueFull()) {


                    System.out.println(Thread.currentThread().getName() + " produced");
                    add2Queue(elementNum--);
                    notifyAll();


                } else {
                    System.out.println("##### -> QUEUE IS FULL");
                    try {
                        wait();

                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }
                }

            }*/
    }
}

/*
    private void add2Queue(int elementNum) {
        queue.offer(elementNum);
    }


    private boolean isQueueFull() {

        if (queue.getSize() == queue.getLimit()) {
            return true;
        }

        return false;
    }*/

