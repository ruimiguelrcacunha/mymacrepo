package org.academiadecodigo.bootcamp.concurrency;

import org.academiadecodigo.bootcamp.concurrency.bqueue.BQueue;

/**
 * Consumer of integers from a blocking queue
 */
public class Consumer implements Runnable {

    private final BQueue<Integer> queue;
    private int elementNum;


    /**
     * @param queue      the blocking queue to consume elements from
     * @param elementNum the number of elements to consume
     */
    public Consumer(BQueue queue, int elementNum) {
        this.queue = queue;
        this.elementNum = elementNum;
    }

    @Override
    public void run() {

        while (elementNum > 0) {
            synchronized (queue) { // sync por causa do sout pois pode nao ser atomico p get size, e se ele parar a meio o get size ja poderá ser diferente da operaçao feita antes
                queue.poll();

                System.out.println("#####" + Thread.currentThread().getName() + " -> elements in queue: " + queue.getSize() + "\n");
                if (queue.getSize()==0){
                    System.out.println(Thread.currentThread().getName() + " has left the queue empty\n");
                }
            }
            --elementNum;
        }



            /*synchronized (queue) {

                if (!isQueueEmpty()) {

                    removeFromQueue();

                    System.out.println(Thread.currentThread().getName() + " consumed 1 ");

                    elementNum--;

                    notifyAll();

                } else {

                    try {
                        System.out.println("aqui ");
                        wait();
                        System.out.println("ainda aqui");
                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }
                }
            }*/
    }
}

   /* public void removeFromQueue() {
        queue.poll();
    }

    public boolean isQueueEmpty() {
        if (queue.getSize() == 0) {
            return true;
        }
        return false;
    }*/

