import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ServerMain {


    private static final int port = 9696;

    public static void main(String[] args) {
        ServerSocket serverSocket = null;
        ExecutorService cachedThreadPool = Executors.newCachedThreadPool();

        try {
            serverSocket = new ServerSocket(port);
            while(true) {

                try {

                    Socket  clientSocket = serverSocket.accept();

                    cachedThreadPool.submit(new TaskReceive(clientSocket));

                } catch (IOException e) {
                    System.out.println("nao apanhou");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
