import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Curl {
    private URL url;

    public Curl() {

        System.out.println("URL: ");
        try {
            this.url = new URL(input());


        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        show();
    }

    private void show() {

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));

            String message = null;

            while ((message = bufferedReader.readLine()) != null) {

                System.out.println(message);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private String input(){
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));


        try {
            return input.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


}
