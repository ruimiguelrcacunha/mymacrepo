import java.util.Arrays;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {

        String message = "I'll send an SOS to the garbage world, " +
                "I hope that someone garbage gets my message in a garbage bottle.";


        Stream<String> messageStream = Arrays.stream(message.split(" "));

        String messageDone = messageStream.filter(string -> !string.equals("garbage")).map(String::toUpperCase).reduce("", (acc, string) -> acc + " " + string + " ");
        System.out.println(messageDone);

        //ou

        String messageDone2 = Stream.of(message.split(" ")).filter(string -> !string.equals("garbage")).map(String::toUpperCase).reduce("", (acc, string) -> acc + " " + string + " ");
        System.out.println(messageDone2);


        // other things


        Cars fiat = new Cars("RED", "FIAT");
        Cars mustang = new Cars("BLUE", "MUSTANG");
        Cars[] cars = {fiat, mustang};

        Stream.of(cars).filter(car -> car.getColor().equals("RED"))


    }
}
