public class Cars {

    String color;
    String brand;

    public Cars(String color, String brand) {
        this.color = color;
        this.brand = brand;
    }


    public String getColor() {
        return color;
    }

    public String getBrand() {
        return brand;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
