import org.academiadecodigo.bootcamp.Prompt;
import org.academiadecodigo.bootcamp.scanners.string.PasswordInputScanner;
import org.academiadecodigo.bootcamp.scanners.string.StringInputScanner;

import java.io.*;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;


public class LogIn {


    private Map<String, String> users = new HashMap();


    public LogIn() {

        logAttempt();
    }

    private void logAttempt() {

        Prompt prompt = new Prompt(System.in, System.out);

        StringInputScanner question1 = new StringInputScanner();
        PasswordInputScanner question2 = new PasswordInputScanner();

        readUsersFile();

        question1.setMessage("User: ");
        question2.setMessage("Password: ");


        // this method will block the thread execution while waiting for user input
        String name = prompt.getUserInput(question1);
        String pass = prompt.getUserInput(question2);



        if(confirmLogInEntry(name, pass)){
            System.out.println("You are in");

        }
        else{
            System.out.println("wrong User or Pass");
        }


    }

    private boolean confirmLogInEntry(String name, String pass) {

        for (int i=0; i<users.size(); i++) {



            if(users.containsKey(name) && users.get(name).equals(pass)){
                return true;

            }


        }
        return false;
    }


    private void readUsersFile() {

        File file = new File("/Users/codecadet/Desktop/MyMacRepo/mymacrepo/Modulo2/PromptViewLogIn/src/Users");
        FileReader fileReader;
        BufferedReader bufferedReader;
        String line = "";
        try {
            fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {

                if (line == null) {
                    bufferedReader.close();
                    break;
                }

                String[] userPass = line.split(":");

                users.put(userPass[0], userPass[1]);


            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


}
