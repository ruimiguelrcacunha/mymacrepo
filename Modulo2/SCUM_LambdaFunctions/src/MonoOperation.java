public interface MonoOperation<T> {

    T monoOperation(T var1);
}
