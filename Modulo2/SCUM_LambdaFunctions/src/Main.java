public class Main {

    public static void main(String[] args) {


        Machine machine = new Machine();

        //estas 2 linhas fazem a mema shit, mas com diferentes sintaxes, basicamente o de baixo e mais easy
        MonoOperation<String> monoString = strVar -> strVar.toUpperCase();
        MonoOperation<String> monoString2 = String::toUpperCase;


        BiOperation<String> biString = (strVar1, strVar2) -> (strVar1 + strVar2);

        MonoOperation<Integer> monoInteger = intVar -> (intVar * 2);
        BiOperation<Integer> biInteger = (intVar1, intVar2) -> (intVar1 + intVar2);




        System.out.println(machine.monoMachine(monoString, "eu"));
        System.out.println(machine.monoMachine(monoString2,"eu"));

        System.out.println(machine.biMachine(biString, "eu ", "me"));


        System.out.println(machine.monoMachine(monoInteger, 2));
        System.out.println(machine.biMachine(biInteger, 2, 8));



    }


}
