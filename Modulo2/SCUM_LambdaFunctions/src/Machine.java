public class Machine<T> {


    public Machine() {

    }

    public T monoMachine(MonoOperation<T> monoOperation, T var) {

        return monoOperation.monoOperation(var);
/*

        String stringVar1 = "eu";
        String stringVar2 = "me";

        MonoOperation<String> monoString = strVar -> (strVar.toUpperCase());
        BiOperation<String> biString = (strVar1, strVar2) -> (strVar1 + strVar2);

        System.out.println(monoString.monoOperation(stringVar1));
        System.out.println(biString.biOperation(stringVar1, stringVar2));
*/


    }


    public T biMachine(BiOperation<T> bi, T var1,T var2) {

        return bi.biOperation(var1,var2);
/*
        int integerVar1 = 1;
        int integerVar2 = 2;

        MonoOperation<Integer> monoInt = intVar -> (intVar * 2);
        BiOperation<Integer> biInt = (intVar1, intVar2) -> (intVar1 + intVar2);

        System.out.println(monoInt.monoOperation(integerVar1));
        System.out.println(biInt.biOperation(integerVar1, integerVar2));*/
    }

}
