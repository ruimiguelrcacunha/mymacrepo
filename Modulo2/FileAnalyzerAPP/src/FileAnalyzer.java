
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Stream;

public class FileAnalyzer {

    public String[] getWords(File file2Use) {

        Optional<String> lines = null;
        try {
            lines = Files.lines(file2Use.toPath()).reduce((line, acc) -> line + acc);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lines.get().split("\\W+");

    }


    public void readAndCountWords(File file2Use) {

        Stream.of(getWords(file2Use)).forEach(word -> System.out.println(word));

        long words = Stream.of(getWords(file2Use)).count();

        System.out.println(words);


    }


    public void longerThan(int longerThanMe, File file2Use) {

        System.out.println(Stream.of(getWords(file2Use))
                .filter(word -> word.length() > longerThanMe)
                .findFirst().get());

    }

    public void longest(int longestNWords, File file) {

        Stream.of(getWords(file))
                .sorted((word1, word2) -> word2.length() - word1.length())
                .limit(longestNWords)
                .forEach(word -> System.out.println(word));

    }

    public void commonWords(File file1, File file2) {

        ArrayList<String> list1 = new ArrayList();
        ArrayList<String> list2 = new ArrayList();
        Set<String> list3 = new HashSet<>();

        Stream file2Stream = Stream.of(getWords(file2));
        file2Stream.forEach(word -> list2.add(word.toString()));

        Stream.of(getWords(file1)).filter(list2::contains).forEach(list3::add);

        Stream.of(list3).forEach(word -> System.out.println(word));


    }
}
