import java.io.*;

public class ReadAndRight {

    private FileReader fileIn;
    private FileWriter fileOut;
    private String line;
    private final String PATH = "/Users/codecadet/Desktop/HowTo_SelfLearned/Read_and_Write_Same_File_Without_Deleting/src/Text";

    public ReadAndRight() throws IOException {
        fileIn = new FileReader(PATH);
        fileOut = new FileWriter(PATH, true); // append permite escrever sem apagar / substituir o ficheiro.
        // Constructs a FileWriter object given a File object. If the second argument is true, then bytes will be written to the end of the file rather than the beginning.
    }

    public void checkIn(String string) throws IOException {

        BufferedReader bufferIn = new BufferedReader(fileIn);

        while ((line = bufferIn.readLine()) != null) {
            System.out.println(line);
        }
        bufferIn.close();

        BufferedWriter bufferOut = new BufferedWriter(fileOut);
        bufferOut.write(string + "\n");

        bufferOut.close();

    }

}
