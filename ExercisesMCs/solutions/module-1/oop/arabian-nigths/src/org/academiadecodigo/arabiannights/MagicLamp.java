package org.academiadecodigo.arabiannights;

import org.academiadecodigo.arabiannights.genies.FriendlyGenie;
import org.academiadecodigo.arabiannights.genies.Genie;
import org.academiadecodigo.arabiannights.genies.GrumpyGenie;
import org.academiadecodigo.arabiannights.genies.RecyclableDemon;

public class MagicLamp {

    private int capacity;
    private int timesRubbed;
    private int timesRecharged;

    public MagicLamp(int capacity) {
        this.timesRubbed = 0;
        this.timesRecharged = 0;
        this.capacity = capacity;
    }

    public Genie rub() {

        System.out.println("*** rub ***");
        timesRubbed++;

        int randomNumWishes = (int) ((Math.random() * 5) + 1);

        if (!hasGenies()) {
            return new RecyclableDemon(randomNumWishes);
        }

        return isEven() ? new FriendlyGenie(randomNumWishes) : new GrumpyGenie(randomNumWishes);
    }

    public void recharge(RecyclableDemon demon) {

        if (demon.isRecycled()) {
            System.out.println("No can do. " + demon + " was already recycled.");
            return;
        }

        demon.recycle();
        timesRecharged++;
        timesRubbed = 0;
        System.out.println("The lamp is being recharged by a " + demon + ".");

    }

    private boolean hasGenies() {
        return timesRubbed <= capacity;
    }

    private boolean isEven() {
        return timesRubbed % 2 == 0;
    }

    public boolean equals(MagicLamp lamp) {
        return this.capacity == lamp.capacity
                && this.timesRubbed == lamp.timesRubbed
                && this.timesRecharged == lamp.timesRecharged;
    }
}