package org.academiadecodigo.arabiannights.genies;

public class Genie {

    private int maxWishes;
    private int wishesGranted;

    public Genie(int maxWishes) {
        this.wishesGranted = 0;
        this.maxWishes = maxWishes;
    }

    public int getNumberOfWishesGranted() {
        return wishesGranted;
    }

    public void grantWish() {

        if (!hasWishesToGrant()) {
            System.out.println(this + " won't grant any more wishes.");
            return;
        }

        System.out.println(this + " has granted your wish. Enjoy the infinite supply of beer, <SHELLmurais_>!");
        wishesGranted++;

    }

    public boolean hasWishesToGrant() {
        return wishesGranted < maxWishes;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}