package org.academiadecodigo.arabiannights;

import org.academiadecodigo.arabiannights.genies.Genie;
import org.academiadecodigo.arabiannights.genies.RecyclableDemon;

public class ArabianNights {

    public static void main(String[] args) {

        MagicLamp aladdin = new MagicLamp(3);

        Genie[] genies = new Genie[5];

        // after this we populate our array by rubbing the lamp
        System.out.println("\n----- Created a lamp, now rubbing it -----"
                + "\n====================================================");

        for (int i = 0; i < genies.length; i++) {
            genies[i] = aladdin.rub();
        }

        System.out.println("\n----- Asking every genie for two wishes -----"
                + "\n====================================================");

        for (Genie genie : genies) {
            genie.grantWish();
            genie.grantWish();
        }

        System.out.println("\n----- Recharging a lamp with a demon  -----"
                + "\n====================================================");

        if(genies[4] instanceof RecyclableDemon) {
            aladdin.recharge((RecyclableDemon) genies[4]);
        }

        System.out.println("\n----- Creating a new lamp -----"
                + "\n====================================================");

        // instantiate another lamp
        MagicLamp willSmith = new MagicLamp(1);

        System.out.println("\n----- Comparing both lamps -----"
                + "\n====================================================");

        // compare the lamps
        System.out.println("The lamps are equal: " + willSmith.equals(aladdin) + "!");

    }

}
