package org.academiadecodigo.arabiannights.genies;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxWishes) {
        super(maxWishes);
    }

    @Override
    public boolean hasWishesToGrant() {
        return getNumberOfWishesGranted() == 0;
    }
}


