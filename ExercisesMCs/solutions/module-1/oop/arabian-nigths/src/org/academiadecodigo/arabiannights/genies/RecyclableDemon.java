package org.academiadecodigo.arabiannights.genies;

public class RecyclableDemon extends Genie {

    private boolean recycled;

    public RecyclableDemon(int maxWishes) {
        super(maxWishes);
        this.recycled = false;
    }

    public boolean isRecycled() {
        return recycled;
    }

    public void recycle() {
        this.recycled = true;
    }

    @Override
    public boolean hasWishesToGrant() {
        return !recycled;
    }

}

