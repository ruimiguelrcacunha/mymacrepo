package org.academiadecodigo.bootcamp;

import java.util.Iterator;

/**
 * UniqueWord entry point
 */
public class Main {

    public static final String STRING = "rui rui rui campelo campelo campelo test abc abc";

    public static void main(String[] args) {

        UniqueWord wc = new UniqueWord(STRING);

        Iterator<String> it = wc.iterator();

        while(it.hasNext()) {
            System.out.println(it.next());
        }

        for (String word: wc) {
            System.out.println(word);
        }

    }


}
