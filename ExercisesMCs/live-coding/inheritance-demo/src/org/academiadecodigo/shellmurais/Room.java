package org.academiadecodigo.shellmurais;

public class Room {

    private boolean available = true;
    private int numberOfBeds;

    public Room(int numberOfBeds) {
        this.numberOfBeds = numberOfBeds;
    }

    public boolean isAvailable() {
        return available;
    }

    public void occupy() {
        available = false;
    }

    public void vacate() {
        available = true;
    }

    public void startRoomService() {
        System.out.println("changing sheets...");
        System.out.println("Vacuuming...");
    }

}
