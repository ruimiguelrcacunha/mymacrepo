package org.academiadecodigo.shellmurais;

public class Playground {

    public static void main(String[] args) {

        SingleRoom singleRoom = new SingleRoom();
        singleRoom.startRoomService();

        Suite suite = new Suite(2);
        Suite suite1 = new Suite(3);
        suite.startRoomService();
        suite.relaxInJacuzzi();

        DoubleRoom doubleRoom = new DoubleRoom();
        doubleRoom.startRoomService();

        Room[] rooms = {singleRoom, suite, doubleRoom};
    }
}
