package org.academiadecodigo.shellmurais;

public class DoubleRoom extends Room {

    public DoubleRoom() {
        super(2);
    }

    @Override
    public void startRoomService() {
        super.startRoomService();
        System.out.println("refilling fridge...");
    }
}
