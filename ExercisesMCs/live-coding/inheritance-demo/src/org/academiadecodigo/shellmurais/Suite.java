package org.academiadecodigo.shellmurais;

public class Suite extends Room {

    public Suite(int numberOfBeds) {
        super(numberOfBeds);
    }

    public void relaxInJacuzzi() {
        System.out.println("A well deserved rest after hitting the head in the table.");
    }

    @Override
    public void startRoomService() {
        super.startRoomService();
        System.out.println("cleaning jacuzzi");
        System.out.println("cleaning balcony...");
    }
}
