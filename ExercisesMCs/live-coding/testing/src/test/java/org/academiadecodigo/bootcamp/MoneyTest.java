package org.academiadecodigo.bootcamp;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.converters.Nullable;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class MoneyTest {

    public static final String VALID_CURRENCY = "EUR";
    public static final int VALID_AMOUNT = 20;

    @Test
    @Parameters({"10, EUR",
            "20, YEN",
            "100, USD",
            "180, WON"})
    public void constructorShouldSetAmountAndCurrency(int amount, String currency) {

        // setup and exercise
        Money money = new Money(amount, currency);

        // verify
        assertEquals(amount, money.getAmount());
        assertEquals(currency, money.getCurrency());
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({"-5",
            "-22",
            "-100"})
    public void constructorShouldThrowExceptionForInvalidAmount(int invalidAmount) {
        new Money(invalidAmount, VALID_CURRENCY);
    }

    @Test(expected = IllegalArgumentException.class)
    @Parameters({"",
            "null"})
    public void constructorShouldThrowExceptionForInvalidCurrency(@Nullable String invalidCurrency) {
        new Money(VALID_AMOUNT, invalidCurrency);
    }

}
