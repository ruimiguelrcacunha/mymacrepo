package org.academiadecodigo.bootcamp;

public class Money {

    private final int amount;
    private final String currency;

    public Money(int amount, String currency) {

        if(amount < 0) {
            throw new IllegalArgumentException("Amount must be zero or higher.");
        }

        if(currency == null || currency.isEmpty()) {
            throw new IllegalArgumentException("Currency must have a value.");
        }

        this.amount = amount;
        this.currency = currency;
    }

    public int getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }
}
