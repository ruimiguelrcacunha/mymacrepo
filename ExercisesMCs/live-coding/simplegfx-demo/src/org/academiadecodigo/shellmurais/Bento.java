package org.academiadecodigo.shellmurais;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Bento {

    private Picture picture;

    public Bento() {
        picture = new Picture(100, 393, "bento.png");
    }

    public void init() {
        picture.draw();
    }
}
