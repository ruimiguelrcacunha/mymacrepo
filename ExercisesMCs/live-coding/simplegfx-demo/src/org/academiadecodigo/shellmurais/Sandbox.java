package org.academiadecodigo.shellmurais;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Sandbox {

    public static void main(String[] args) {

        Rectangle rectangle = new Rectangle(10, 10, 800, 800);
        rectangle.setColor(Color.ORANGE);

        rectangle.fill();
        // rectangle.translate(100, 200);

        /*Rectangle anotherRectangle = new Rectangle(300, 300, 200, 200);
        anotherRectangle.setColor(Color.BLUE);

        anotherRectangle.fill();*/

        Bento bento = new Bento();
        bento.init();

        Ganso ganso = new Ganso();
        ganso.init();
    }
}
