package org.academiadecodigo.shellmurais.concurrency.explicit;

public class ThreadLauncher {

    public static void main(String[] args) {

        MyTask myTask = new MyTask();
        myTask.run();

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new MyTask());
            thread.setName("Thread-" + i);
            thread.start();
        }

        Thread anotherThread = new Thread(myTask);
        anotherThread.start();
    }
}
