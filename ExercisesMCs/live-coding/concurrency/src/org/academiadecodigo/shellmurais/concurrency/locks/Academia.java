package org.academiadecodigo.shellmurais.concurrency.locks;

public class Academia {

    public static void main(String[] args) {

        Bathroom bathroom = new Bathroom();

        Thread igreja = new Thread(new Cadet(bathroom, "Igreja"));
        Thread carneiro = new Thread(new Cadet(bathroom, "Carneiro"));
        Thread ze = new Thread(new Cadet(bathroom, "Zé"));

        igreja.start();
        carneiro.start();
        ze.start();
    }
}
