package org.academiadecodigo.shellmurais.concurrency.synchronization;

public class FabricaDasCaldas {

    public static void main(String[] args) {

        System.out.println("Gerente: À espera que estes colaboradores acabem...");

        Thread trabalhador = new Thread(new TrabalhadorDasCaldas());
        trabalhador.start();

        try {

            trabalhador.join();
            System.out.println("Gerente: Finalmente acabaram, vou abandonar!");

        } catch (InterruptedException e) {

            System.out.println("Gerente: Acordaram-me da sesta...");
            e.printStackTrace();
        }

    }
}
