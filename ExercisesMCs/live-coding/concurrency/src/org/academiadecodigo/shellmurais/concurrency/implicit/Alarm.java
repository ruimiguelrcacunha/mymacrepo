package org.academiadecodigo.shellmurais.concurrency.implicit;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class Alarm {

    private Timer timer;

    public static void main(String[] args)  {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Number of times to ring: ");
        int numRings = Integer.parseInt(scanner.next());

        System.out.println("Ring interval: ");
        int ringInterval = scanner.nextInt();

        System.out.println(Thread.currentThread().getName());

        Alarm alarm = new Alarm();
        alarm.start(ringInterval, numRings);

        Alarm anotherAlarm = new Alarm();
        anotherAlarm.start(ringInterval, numRings);
    }

    private void start(int ringInterval, int numRings) {
        timer = new Timer();

        timer.scheduleAtFixedRate(new Ring(numRings), ringInterval * 1000, ringInterval * 1000);
    }

    private class Ring extends TimerTask {

        private int numRings;

        public Ring(int numRings) {
            this.numRings = numRings;
        }

        @Override
        public void run() {

            System.out.println(Thread.currentThread().getName());
            System.out.println("Alarm is ringing...");

            numRings--;

            if(numRings == 0) {
                System.out.println("Alarm cancelled...");
                stop();
            }
        }

        private void stop() {
            timer.cancel();
        }
    }
}
