import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");
        EntityManager em = emf.createEntityManager();

        System.out.println("Result: " + em.createNativeQuery("SELECT 1+1").getSingleResult());


        Film theShinning = new Film();
        //transien
        theShinning.setDirector("S. Kubrick");
        theShinning.setTitle("The Shinning");
        theShinning.setRating(10);
        theShinning.setId(1);

        em.getTransaction().begin();
        em.persist(theShinning);
        //persisted
        em.getTransaction().commit();

        //detached
    }
}
