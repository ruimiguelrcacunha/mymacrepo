package game.grid;

import game.game.Game;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Grid {

    private Rectangle grid;

    public Grid() {
        init();
    }

    private void init() {
        grid = new Rectangle(Game.PADDING, Game.PADDING, Game.col2X(Game.WIDTHROWS), Game.row2Y(Game.HEIGHTCOL));
        grid.setColor(Color.BLACK);
        grid.fill();

    }


    public int getGridX() {
        return grid.getX();
    }

    public int getgridY() {
        return grid.getY();
    }

    public int getGridHeight() {
        return grid.getHeight();
    }

    public int getGridWidth() {
        return grid.getWidth();
    }


}
