package game.inputs;

public interface ToDo {

    void actionPressed(int key);

    void actionReleased(int key);

}


