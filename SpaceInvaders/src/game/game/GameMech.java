package game.game;

import game.enemy.Bino;
import game.enemy.Enemy;

import java.util.ArrayList;
import java.util.HashMap;


public class GameMech {


    public ArrayList<Enemy> enemyCreation() {

        ArrayList<Enemy> enemyList = new ArrayList<>();

        int posX = (Game.WIDTHROWS / Game.CELL_SIZE);
        int posY = Game.HEIGHTCOL / 6;

        for (int i = 0; i < Game.ENEMY_NUMBER; i++) {
            Enemy enemy = new Bino(Game.row2Y(posX), Game.col2X(posY));

            enemyList.add(enemy);
            enemy.draw();
            posX += Game.CELL_SIZE;
        }
        return enemyList;
    }

    private boolean borderVerificationRight(int posx, int speed) {

        if (posx + speed >= Game.row2Y(Game.WIDTHROWS) - Game.PADDING * 2) {
            return true;
        }
        return false;
    }

    private boolean borderVerificationLeft(int posx, int speed) {

        if (posx - speed <= Game.PADDING) {
            return true;
        }
        return false;
    }

    //setting directions of enemies
    public void update(ArrayList<Enemy> enemyList) {

        boolean change2Right = false;
        boolean change2Left = false;

        if (borderVerificationRight(enemyList.get(enemyList.size() - 1).getPosX(), enemyList.get(enemyList.size() - 1).getSpeed())) {
            change2Left = true;
        }
        if (borderVerificationLeft(enemyList.get(0).getPosX(), enemyList.get(0).getSpeed())) {
            change2Right = true;
        }

        if (change2Right) {
            for (Enemy enemy : enemyList) {
                enemy.setDirectionRight();
            }
        }
        if (change2Left) {
            for (Enemy enemy : enemyList) {
                enemy.setDirectionLeft();
            }
        }
    }


    public void updateRays(ArrayList<Ray> rays) {
        for (Ray ray : rays) {
            ray.moveUp();

            if (ray.getPosY() <= 10) {
                ray.deleteSelf();

            }
        }

    }

    //check if ray meets alien and returns a map with those objects
    public HashMap<Ray, Enemy> checkTargetedAliens(ArrayList<Ray> rays, ArrayList<Enemy> enemyList) {


        HashMap<Ray, Enemy> map = new HashMap<>();

        for (Ray ray : rays) {
            for (Enemy enemy : enemyList) {
                if ((ray.getPosX() == enemy.getPosX()) && (ray.getPosY() == enemy.getPosY())) {
                    map.put(ray, enemy);
                }
            }
        }
        return map;
    }






}