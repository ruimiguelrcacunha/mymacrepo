package game.game;

import game.enemy.Bino;
import game.enemy.Enemy;
import game.grid.Grid;
import game.hero.Hero;
import game.inputs.Inputs;
import game.inputs.ToDo;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Game implements ToDo {

    public final static int PADDING = 10;
    public final static int HEIGHTCOL = 100;
    public final static int WIDTHROWS = 70;
    public final static int CELL_SIZE = 10;
    public final static int ENEMY_NUMBER = 4;


    private ArrayList<Enemy> enemyList;
    private Executor executor = Executors.newSingleThreadExecutor();

    private Grid grid;
    private Inputs inputs;
    private GameMech gameMech;
    private Hero hero;
    private ArrayList<Ray> rays = new ArrayList<>();

    private boolean gameOver = false;


    public Game() {

        inputs = new Inputs();
        inputs.setInputScreen(this);

        gameMech = new GameMech();
        enemyList = new ArrayList<>();


        init();
    }

    private void init() {
        grid = new Grid();
        enemyList = gameMech.enemyCreation();
        hero = new Hero();

        start();
    }


    private void start() {

        while (!gameOver) {

            gameMech.update(enemyList);
            gameMech.updateRays(rays);
            updateLists(gameMech.checkTargetedAliens(rays, enemyList));

            for (Enemy enemy : enemyList) {
                enemy.moveRorL();


            }
            try {
                Thread.sleep(5);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }

    public void removeFromList(Ray ray, Enemy enemy) {
        rays.remove(ray);
        enemyList.remove(enemy);
    }

    public void updateLists(HashMap<Ray, Enemy> map) {
        if (map.isEmpty()) {
            return;
        }
        map.forEach((ray, enemy) -> removeFromList(ray, enemy));
    }


    public static int row2Y(int row) {
        return row * CELL_SIZE;
    }

    public static int col2X(int col) {
        return col * CELL_SIZE;
    }


    @Override
    public void actionPressed(int key) {

        switch (key) {
            case KeyboardEvent.KEY_UP:
                break;
            case KeyboardEvent.KEY_DOWN:
                break;
            case KeyboardEvent.KEY_RIGHT:
                hero.setMoveRightTrue();

                break;

            case KeyboardEvent.KEY_LEFT:
                hero.setMoveLeftTrue();

                break;

            case KeyboardEvent.KEY_SPACE:
                rays.add(hero.ray());// tem q levar cooldown
                break;

        }
    }

    @Override
    public void actionReleased(int key) {


    }
}
