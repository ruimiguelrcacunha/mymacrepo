package game.game;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Ray {


    private Rectangle ray;
    private int posX;
    private int posY;

    private final int rayPadding = 7;
    private final int rayHeight = 5;
    private final int rayWidth = 5;


    public Ray(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;

        ray = new Rectangle(posX + rayPadding, posY, rayWidth, rayHeight);
        ray.setColor(Color.RED);
        ray.fill();
    }


    public void moveUp() {
        ray.translate(0, -1);
        posY = ray.getY();
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    public void deleteSelf(){
        ray.delete();
    }


}
