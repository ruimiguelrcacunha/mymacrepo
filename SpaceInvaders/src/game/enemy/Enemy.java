package game.enemy;

import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public abstract class Enemy implements Runnable, Shootable {

    protected int health;
    protected int posY;
    protected int posX;
    protected int speed;
    protected EnemyType enemyType;
    protected int ENEMYBOX_SIZE = 20;
    protected Direction direction = Direction.RIGHT;


    Rectangle enemyFigure;
    Color color;

    public Enemy(EnemyType enemyType, int col, int row) {
        this.enemyType = enemyType;
        this.health = enemyType.getHealth();
        this.speed = enemyType.getSpeed();
        this.color = enemyType.getColor();
        this.posY = row;
        this.posX = col;
        enemyFigure = new Rectangle(posX, posY, ENEMYBOX_SIZE, ENEMYBOX_SIZE);
        enemyFigure.setColor(color);
    }


    public void draw() {
        enemyFigure.fill();
    }

    @Override
    public void moveRorL() {

        if (direction == Direction.RIGHT) {
            enemyFigure.translate(speed, 0);
            posX = enemyFigure.getX();

        }
        if (direction == Direction.LEFT) {
            enemyFigure.translate(-speed, 0);
            posX = enemyFigure.getX();
        }


    }

    public void setDirectionRight() {
        this.direction = Direction.RIGHT;
    }

    public void setDirectionLeft() {
        this.direction = Direction.LEFT;
    }

    public int getPosY() {

        return posY;
    }

    public int getPosX() {
        return posX;
    }

    public int getSpeed() {
        return speed;
    }

    public Direction getDirection() {
        return direction;
    }
}
