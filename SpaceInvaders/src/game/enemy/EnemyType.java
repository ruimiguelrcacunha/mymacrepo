package game.enemy;

import org.academiadecodigo.simplegraphics.graphics.Color;

public enum EnemyType {

    BINO(1, 10, Color.BLUE),
    ZE(2, 50, Color.ORANGE);

    private final int speed;
    private final int health;
    private final Color color;

    EnemyType(int speed, int health, Color color) {
        this.speed = speed;
        this.health = health;
        this.color = color;
    }

    public int getSpeed() {
        return speed;
    }

    public int getHealth() {
        return health;
    }

    public Color getColor() {
        return color;
    }
}
