package game.hero;

import game.enemy.Shootable;
import game.game.Game;
import game.game.Ray;
import org.academiadecodigo.simplegraphics.graphics.Color;
import org.academiadecodigo.simplegraphics.graphics.Line;
import org.academiadecodigo.simplegraphics.graphics.Rectangle;

public class Hero {

    private int health = 100;
    private int speed = 15;
    private final int boxSize = 20;
    private boolean move = true;
    private boolean moveLeftTF = false;
    private boolean moveRightTF = false;

    private final int time2Move = 15;

    private Rectangle heroFig;



    public void setMoveLeftFalse() {
        this.moveLeftTF = false;
    }

    public void setMoveRightFalse() {
        this.moveRightTF = false;
    }

    public void setMoveLeftTrue() {
        this.moveLeftTF = true;
    }

    public void setMoveRightTrue() {
        this.moveRightTF = true;
    }

    public Hero() {
        initHeroFig();
        moveThread.start();
    }

    private void initHeroFig() {

        heroFig = new Rectangle(Game.row2Y(Game.WIDTHROWS / 2), Game.col2X(Game.HEIGHTCOL - (Game.HEIGHTCOL / 10)), boxSize, boxSize);
        heroFig.setColor(Color.GREEN);
        heroFig.fill();

    }


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    public Ray ray(){
        return new Ray(heroFig.getX(),heroFig.getY());

    }




//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    private boolean borderCheckLeft() {
        if (heroFig.getX() - speed <= Game.PADDING) {
            return true;
        }
        return false;
    }

    private boolean borderCheckRight() {
        if (heroFig.getX() + speed >= Game.row2Y(Game.WIDTHROWS) - Game.PADDING * 2) {
            return true;
        }
        return false;
    }

    public void moveLeft() {
        if (borderCheckLeft()) {
            return;
        }
        heroFig.translate(-1, 0);
    }
    public void moveRight() {

        if (borderCheckRight()) {
            return;
        }
        heroFig.translate(1, 0);
    }
    Thread moveThread = new Thread(new Runnable() {
        private void left(){
            for (int i = 0; i < speed ; i++) {
                try {
                    Thread.sleep(time2Move);
                    moveLeft();
                } catch (InterruptedException e) {
                    System.out.println("moveLeft deu falhou");
                }
            }

        }

        private void right(){
            for (int i = 0; i < speed ; i++) {
                try {
                    Thread.sleep(time2Move);
                    moveRight();
                } catch (InterruptedException e) {
                    System.out.println("moveRight deu falhou");
                }
            }
        }

        @Override
        public void run() {

            while (move) {

                if (moveLeftTF) {
                    left();
                    setMoveLeftFalse();
                }
                if (moveRightTF){
                    right();
                    setMoveRightFalse();
                }

                System.out.println("on Going");
            }
        }
    });


}
