import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

public class WordHistogramComposition implements Iterable<String>{
    //Composition

    private String STRING;
    private Map<String, Integer> map = new HashMap<>();

    public WordHistogramComposition(String STRING) {
        this.STRING = STRING;

        splitAddThatShit();
    }

    private void splitAddThatShit(){

        String[] splitString= STRING.split(" ");

        for (String str: splitString) {
            if(!map.containsKey(str)){
                map.put(str,1);
                continue;
            }
            int numberOfOcurrences = map.get(str);
            map.replace(str,++numberOfOcurrences);
        }

    }

    public int size(){
        return map.size();
    }

    public int get(String key){

        return map.get(key);
    }

    @Override
    public Iterator<String> iterator() {
        return map.keySet().iterator();
    }
}
