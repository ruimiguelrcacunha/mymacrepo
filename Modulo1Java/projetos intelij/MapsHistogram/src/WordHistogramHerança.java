import java.util.Iterator;
import java.util.TreeMap;

public class WordHistogramHerança extends TreeMap<String, Integer> implements Iterable<String> {
    //usar herança


    String STRING;

    public WordHistogramHerança(String STRING) {
        this.STRING = STRING;

        addAndSplitThisShit();
    }

    private void addAndSplitThisShit(){

        String[] str = STRING.split(" ");

        for (String word: str) {
            if(!containsKey(word)){
                put(word,1);
                continue;
            }
            int numberOfOccurences = get(word);
            replace(word,++numberOfOccurences);
        }
    }


    @Override
    public Iterator<String> iterator() {
        return keySet().iterator();
    }
}
