public class Game {
    int max;
    int min;
    private int nPlayers;


    public Game(int max, int min, int nPlayers){
        this.max=max;
        this.min=min;
        this.nPlayers=nPlayers;
    }


    public void start(){

        Randomizer rand = new Randomizer();
        Validation validation = new Validation();
        int correct = rand.random(min,max);
        boolean guess=false;

        while(!guess){

            for (int i=0; i<nPlayers;i++){
                System.out.println("Player "+ i +":");
                int pick=rand.random(min,max);
                guess=validation.check(correct,pick);

            }
        }
    }


}