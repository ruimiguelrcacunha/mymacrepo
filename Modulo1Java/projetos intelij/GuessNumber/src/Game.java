public class Game {
    int min;
    int max;
    int correctPick;
    double pick = 1000;
    int counter = 0;
    public Game (int min, int max, int correctPick){
        this.min=min;
        this.max=max;
        this.correctPick=correctPick;
    }
    Random random = new Random();
    public void start() {
        Player player1 = new Player("Tone");
        Player player2 = new Player("Bino");
        Guess guess = new Guess();
        while (correctPick != pick) {
            if (counter % 2 != 0) {
                player1.setPick(random.gen(min, max));
                pick = player1.getPick();
                guess.printGuess(pick, correctPick, player1.getName());
            } else {
                player2.setPick(random.gen(min, max));
                pick = player2.getPick();
                guess.printGuess(pick, correctPick, player2.getName());
            }
            counter++;
        }
    }
}