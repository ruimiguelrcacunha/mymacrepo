public class Player {
    private String name;
    public Player(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    private double pick;
    public void setPick(double number){
        pick=number;
    }
    public double getPick(){
        return pick;
    }
}