public class Random {
    public double gen (int min,int max){
        double numberGen=Math.round(Math.random()*100);
        while (numberGen>max || numberGen<min){
            numberGen=Math.round(Math.random()*100);
        }
        return numberGen;
    }
}