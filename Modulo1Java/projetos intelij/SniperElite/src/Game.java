import gameObjects.Enemys.ArmouredEnemy;
import gameObjects.Enemys.Enemy;
import gameObjects.GameObjects;

import gameObjects.Objects;
import gameObjects.Tree;

public class Game {
    //variables
    private int numberOfGameObjects;
    private GameObjects[] gameObjects;
    private SniperRifle weapon;
    private int shotsFired = 0;

    //class constructor
    public Game(){
        this.numberOfGameObjects = Randoms.numberOfGameObjects();
        this.gameObjects = new GameObjects[numberOfGameObjects];
        this.weapon = new SniperRifle();
    }

    public void start() {

        createObjects();

        for (GameObjects obj : gameObjects) {
            if (obj instanceof Tree) {
                System.out.println("\n" + obj.getMessage() + "\n");
                continue;
            }

            System.out.println("\n***** " + obj.getMessage() + " *****");
            if(((Enemy) obj) instanceof ArmouredEnemy){((ArmouredEnemy) obj).showArmorBar();}
            ((Enemy) obj).showHealthBar();


            while (!((Enemy) obj).isDead()) {

                weapon.shoot((Enemy) obj);
                shotsFired++;

            }
            System.out.println("DEAD!!");
            System.out.println("\nYou shot " + shotsFired + " times!\n********************");
        }
    }



    private void createObjects(){
        for(int i=0 ; i<numberOfGameObjects; i++ ){
            gameObjects[i]=Randoms.objects();

        }
    }



}
