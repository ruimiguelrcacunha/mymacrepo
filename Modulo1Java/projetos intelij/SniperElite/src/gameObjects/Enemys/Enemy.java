package gameObjects.Enemys;


import gameObjects.GameObjects;
import gameObjects.Objects;


public abstract class Enemy extends GameObjects {


    public Enemy(EnemyType enemyType){
        super(Objects.ENEMY);
        this.enemyType = enemyType;
    }
//initial variables
    private  int health = 100;
    private boolean isDead = false;
    private EnemyType enemyType;

//getters and setters
    public boolean isDead() {
        return isDead;
    }
    public void setDead() {
        isDead = true;
    }
    public int getHealth() {
        return health;
    }
    public void healthDown(int damage) {
        this.health = health - damage;
        if(this.health <= 0){this.health=0;}
    }

    public EnemyType getEnemyType() {
        return enemyType;
    }


    //methods
    public abstract void hit(int shot);

    public void showHealthBar(){
        String healthBar="";
        for (int i = 0; i<getHealth(); i+=5){
            healthBar = healthBar + "|";
        }
        if(getHealth()>0){System.out.println("HP: " + healthBar + " " + health);}
    }

}
