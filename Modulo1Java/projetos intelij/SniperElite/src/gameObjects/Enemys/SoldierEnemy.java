package gameObjects.Enemys;

public class SoldierEnemy extends Enemy{

    public SoldierEnemy(){
        super(EnemyType.SOLDIER);
    }

    public String getMessage(){
        return "Soldier";
    }
    @Override
    public void hit(int shot){

        healthDown(shot);
        showHealthBar();
       // System.out.println("Health: " + getHealth() + "\n");
        if(getHealth()<=0){
            setDead();
        }
    }


}
