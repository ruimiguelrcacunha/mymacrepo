import gameObjects.Enemys.BodyParts;
import gameObjects.Enemys.Enemy;

public class SniperRifle {


    public void shoot(Enemy enemy){

        BodyParts bodyPart =  Randoms.bodyPartHit();

        int shot;
        switch (bodyPart){

            case HEAD:
                shot = Randoms.headshotDamage();
                System.out.println("\nHeadShot!  Damage: " + shot );
                enemy.hit(shot);
                return;

            case CHEST:
                shot = Randoms.chestshotDamage();
                System.out.println("\nChestShot!  Damage: " + shot);
                enemy.hit(shot);
                return;

            case LEGS:
                shot = Randoms.legshotDamage();
                System.out.println("\nLegShot!  Damage: " + shot);
                enemy.hit(shot);

            case MISS:
                //System.out.println("\nShot missed!");
                //if(enemy instanceof ArmouredEnemy){
                //    ((ArmouredEnemy) enemy).armorBar();
               // }
               // enemy.showHealthBar();

        }
    }
}
