public class ToDoItem implements Comparable<ToDoItem>{

    private final Importance importance;
    private final String text;
    private final int priority;


    public ToDoItem(Importance importance, int priority, String text){
        this.importance = importance;
        this.priority = priority;
        this.text=text;

    }

    public Importance getImportance() {
        return importance;
    }

    public String getText() {
        return text;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return text;
    }

    @Override
    public int compareTo(ToDoItem el2) {
        switch (this.getImportance()){
            case HIGH:
                if (el2.getImportance().equals(Importance.HIGH)){
                    break;
                }
                return -1;

            case MEDIUM:
                if(el2.getImportance().equals(Importance.MEDIUM)){
                    break;
                }
                if(el2.getImportance().equals(Importance.LOW)){
                    return -1;
                }
                return 1;

            case LOW:
                if(el2.getImportance().equals(Importance.LOW)){
                    break;
                }
                return 1;
        }
        if(this.getPriority() > el2.getPriority()){
            return 1;
        }
        if(this.getPriority() < el2.getPriority()){
            return -1;
        }
        return 0;

    }
}
