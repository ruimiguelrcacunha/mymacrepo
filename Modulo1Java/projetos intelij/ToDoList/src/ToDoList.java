

import java.util.PriorityQueue;

public class ToDoList {


    PriorityQueue<ToDoItem> toDoList = new PriorityQueue<>();


    public void add(ToDoItem toDoItem) {
        toDoList.add(toDoItem);
    }

    public boolean isEmpty() {
        return toDoList.isEmpty();
    }

    public ToDoItem remove() {
        return toDoList.remove();
    }


}



