import animals.Animal;
import animals.Chicken;
import animals.Cow;
import animals.Pig;

public class Farm {

private int pigCount=0;
private int cowCount=0;
private int chickenCount=0;
private boolean farmStatus=true;

    public boolean getFarmStatus() {
        return farmStatus;
    }

    public Animal buy(){
        if (farmStatus) {
            return fetchAnimal();
        }
        return null;

    }

    private Animal fetchAnimal(){

       int animalProb = (int)(Math.random()*101);

        if (animalProb<5){

            pigCount++;
            return new Pig();
        }
        else if(animalProb>=5 && animalProb<50){

            cowCount++;
            return new Cow();
        }
        chickenCount++;
        return new Chicken();
    }
    public boolean influence (Pig pig){
       return farmStatus=false;

    }

}
