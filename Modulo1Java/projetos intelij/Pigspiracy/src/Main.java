import animals.Animal;
import animals.Pig;

public class Main {
    public static void main(String[] args) {

    Farm farm = new Farm();
    Animal[] animalsBought = new Animal[100];
    int i=0;

        while(farm.getFarmStatus() && i<100){
            i++;

            animalsBought[i] = farm.buy();
            animalsBought[i].talk();

            if(animalsBought[i] instanceof Pig) {

                farm.influence((Pig)(animalsBought[i]));
                System.out.println("Pigs have taken CONTRRROOOOOOOOOOLLLLLLLLL!!");

            }
        }
    }
}
