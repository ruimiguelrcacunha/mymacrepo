public class Lamp {



    private int maxGenies;
    private int timesRecharged=0;
    private int genieCount=0;
    private int timesRubbed=0;
    private boolean recharge = false;

    public Lamp(){
        maxGenies=RandomSetter.maxGenieRandomizer();
    }


    public void rub(){

        if(timesRubbed < maxGenies){
            if(timesRubbed%2 == 0){
                FriendlyGenie friendlyGenie = new FriendlyGenie();
                recharge = friendlyGenie.summon();
                genieCount++;
            }
            else{
                GrumpyGenie grumpyGenie = new GrumpyGenie();
                recharge = grumpyGenie.summon();
                genieCount++;
            }

        }
        else{
            RecyclableDemon recyclableDemon = new RecyclableDemon();
            recharge = recyclableDemon.summon();
        }
        timesRubbed++;
        if(recharge==true){
            timesRecharged++;
            recharge=false;
            genieCount=0;
            maxGenies=RandomSetter.maxGenieRandomizer();
        }
    }

    public void lampInfo(){
        int geniesLeft = maxGenies-genieCount;
        if(geniesLeft<0){geniesLeft=0;genieCount=0;}
        System.out.println("Initial number of genies in lamp : " + maxGenies + ".\nGenies remaining " + (geniesLeft) + ".\nTimes recharged " + timesRecharged + ".\n\n");
    }
}
