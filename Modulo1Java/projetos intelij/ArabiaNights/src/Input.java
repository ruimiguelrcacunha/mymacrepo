import java.sql.SQLOutput;
import java.util.Scanner;

public class Input {
    static Scanner scan = new Scanner(System.in);

    public static boolean yesNo(){
        String answer = scan.next();
        while (!answer.toUpperCase().equals("NO") && !answer.toUpperCase().equals("YES") && !answer.toUpperCase().equals("N") && !answer.toUpperCase().equals("Y")){
            System.out.println("Repeat answer.");
            answer = scan.next();
        }
        if(answer.toUpperCase().equals("NO") || answer.toUpperCase().equals("N")){
            return false;
        }
        else{return true;}
    }
    public static String wish(){
        String answer = scan.next();
        return answer;

    }

    public static int choice(){
        int answer = scan.nextInt();
        while(answer != 1 && answer != 2 && answer != 3 ){
            System.out.println("Repeat!");
                answer = scan.nextInt();
        }
        return answer;
    }

    public static int mainMenu(){
        System.out.println("What to do?\n1) Rub\n2) Lamp info\n3) Exit");
        int answer = scan.nextInt();
        while (answer != 1 && answer !=2 && answer !=3){
            answer = scan.nextInt();
        }
        return answer;
    }

}
