public class FriendlyGenie extends Genie{

    public FriendlyGenie(){
        super(GenieType.FRIENDLY);
    }

    public boolean summon(){
        System.out.println("Hello! I see you have summoned me! I am a Friendly Genie. You are in luck");
        return AskForAWish();
    }

    private boolean AskForAWish(){
        System.out.println("You have " + (getWishes()-getWishCount()+1) +" wishes. What do you Wish? Begin with wish with \"I wish for, ... \"");
        String wishToBeApproved = Input.wish();




        if(getWishCount()<getWishes()){
            System.out.println("Wish granted!");
            incrementWishCount();
            System.out.println("Do you wish to ask another one? If you decline i will vanish and you will never see me again for more wishes!!\nThink wisely!");
            boolean answer = Input.yesNo();
            if(answer == true){
                AskForAWish();
            }
            else{return false;}
        }
        else{
            System.out.println("You don't have any more wishes!");
            return false;
        }
        return false;

    }



}
