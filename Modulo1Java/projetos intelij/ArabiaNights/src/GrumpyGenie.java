public class GrumpyGenie extends Genie{

    public GrumpyGenie(){
        super(GenieType.GRUMPY);
    }

    public boolean summon(){
        System.out.println("Hello! I see you have summoned me! I am Grumpy Genie");
        return AskForAWish();
    }
    private boolean AskForAWish() {
        System.out.println("You have " + (getWishes()-getWishCount()+1) +" wishes. What do you Wish? Begin with wish with \"I wish for, ... \"");
        String wishToBeApproved = Input.wish();
        if (getWishCount() < 1) {
            System.out.println("Wish granted!");
            incrementWishCount();
            System.out.println("Do you wish to ask another one?");
            boolean answer = Input.yesNo();
            if (answer) {
                System.out.println("Never mind, I changed my mind! I will not grant you anymore wishes!");
                return false;
            } else {
                return false;
            }
        } else {
            System.out.println("I will not grant anymore wishes!");
            return false;
        }
    }
}
