

import java.util.Scanner;
public class Genie {

    private int wishes;
    private GenieType genieType;
    private int wishCount=0;

    public int getWishCount() {
        return wishCount;
    }

    public int getWishes() {
        return wishes;
    }

    public void incrementWishCount() {
        this.wishCount++;
    }

    public Genie(GenieType genieType){
        this.wishes=RandomSetter.maxWishesRandomizer();//set number of wishes
        this.genieType = genieType;
    }








}
