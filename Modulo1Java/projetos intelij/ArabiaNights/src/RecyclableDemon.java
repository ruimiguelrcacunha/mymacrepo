public class RecyclableDemon extends Genie{


    private boolean recycled=false;


    public RecyclableDemon(){
        super(GenieType.RECYCLABLE);
    }

    public boolean isRecycled() {
        return recycled;
    }

    public void setRecycledTrue() {
        this.recycled = true;
    }

    public void setRecycledFalse() {
        this.recycled = false;
    }


    public boolean summon(){
        System.out.println("Hello! I see you have summoned me! I am a RECYCLABLE DEMON");
        return AskForAWish();
    }

    private boolean AskForAWish() {

        if (isRecycled() == false ){
            System.out.println("What do you Wish? Begin with wish with \"I wish for, ... \"");//tentar regex
            String wishToBeApproved = Input.wish();
            System.out.println("Wish granted!");
            incrementWishCount();
        }
        return choice();


    }

    private boolean choice(){
        System.out.println("What you want nex?\n1) Another wish\n2) You can recycle me and recharge the lamp\n3) Let me go");
        int answer=Input.choice();
        if(answer==1){
            if(isRecycled()==true){
                System.out.println("I can't grant anymore wishes, just let me go!!");
                choice();
            }
            AskForAWish();
       }
       else if(answer==2){
            if(isRecycled()==true){
               System.out.println("I'm already recycled, JUST LET ME GO!!");
           }
           return recycle();
       }
       else{return false;}
       return false;
    }

    private boolean recycle(){
        setRecycledTrue();
        return true;
    }
}
