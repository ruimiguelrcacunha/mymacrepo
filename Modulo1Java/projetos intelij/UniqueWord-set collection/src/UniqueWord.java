import java.util.*;
import java.util.function.Consumer;

public class UniqueWord implements Iterable<String> {

    private String string;
    private Set<String> set ;


    public UniqueWord(String string){
        this.string = string;

      set = new TreeSet<>();
      split();
    }

    private void split(){

        String [] strs = string.split(" ");

        for (String st : strs) {
            set.add(st);

        }
    }

    @Override
    public Iterator<String> iterator() {
        return set.iterator();
    }
}
