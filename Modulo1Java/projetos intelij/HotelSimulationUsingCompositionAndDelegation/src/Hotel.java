public class Hotel {


    private String hotelName;

    private Room[] rooms; // Hotels has rooms


    public Hotel(int numberOfRooms, String hotelName){
        this.hotelName = hotelName;
        this.rooms = new Room[numberOfRooms];//criaçao do array com numberOfRooms de rooms, mas vazio
        for(int i = 0; i<numberOfRooms; i++){//atribuiçao de cada espaço no array a um objeto room.
            rooms[i]=new Room(i+1);
        }
    }



    public Key checkIn(String name, Person guestIn){
        // verify if rooms available, if yes book it

        for (Room room : rooms) {//check se o objeto person ja entrou no sistema
            if (room.getGuest() == null) {
                continue;
            }
            if (room.getGuest().equals(guestIn)) {
                System.out.println(name + ", you have already checked in!\n");
                return Key.DUPLICATED;
            }
        }
        for (Room room : rooms){
            if(room.isVacant() == true){
                System.out.println("There is one room for you! Room number: " + room.getRoomNumber());

                room.setVacantFalse();
                room.setName(name);
                room.setGuest(guestIn);

                System.out.println("Here are the keys, " + name + "! enjoy your stay!\n");
                return Key.TRUE;
            };
        }
        //if not dont book it
        System.out.println("No rooms available try other place!\n");
        return Key.FALSE;//no key (key== false)

    }
    public Key checkOut(String name, Person guestOut){


         for(Room room : rooms){
             if(room.getGuest() == null){continue;}
               if(room.getGuest().equals(guestOut)){
                   room.resetName();
                   room.setVacantTrue();
                   room.resetGuest();
                   System.out.println("See you next time " + name + "!\n");
                   return Key.FALSE;
               }
         }
        System.out.println("You dont have a room here!\n");
         return Key.FALSE;
    }

    public void seeGuests(){
        boolean isempty = true;
        System.out.println("Guests in \"" + hotelName + "\" are: ");

        for (Room room : rooms){
            if(room.isVacant() == false)
                System.out.println(room.getName() + " in room " + room.getRoomNumber() + ".");
                isempty=false;
        }
        if (isempty){
            System.out.println("Sorry but " + hotelName + " is empty!");
        }
        System.out.println("\n");
    }

}


