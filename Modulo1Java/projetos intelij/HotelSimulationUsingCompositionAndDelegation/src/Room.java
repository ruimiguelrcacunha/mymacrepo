public class Room {

    private boolean vacant = true;
    private String name ;
    private int roomNumber;
    private Person guest = null;

    public Room(int roomNumber){
        this.roomNumber=roomNumber;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public boolean isVacant() {
        return vacant;
    }
    public void setVacantTrue() {
        this.vacant = true;
    }
    public void setVacantFalse() {
        this.vacant = false;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void resetName() {
        this.name = null;
    }

    public Person getGuest() {
        return guest;
    }

    public void setGuest(Person guest) {
        this.guest = guest;
    }
    public void resetGuest() {
        this.guest = null;
    }

}
