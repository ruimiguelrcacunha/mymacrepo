public class Main {

    public static void main(String[] args){

        Hotel hotel1 = new Hotel(3,"EstaleiroINN");
        Hotel hotel2 = new Hotel(10,"10RoomsForRent");
        Hotel[] hotels ={hotel1, hotel2};
        Person person1 = new Person("Zé");
        Person person2 = new Person("Bino");
        Person person3 = new Person("Bino");
        Person person4 = new Person("Tone");

        person1.tryCheckIn(null);
        person1.tryCheckIn(hotels[0]);

        person1.tryCheckIn(hotels[0]);
        person2.checkOut(hotels[0]);
        person2.tryCheckIn(hotels[0]);
        person3.tryCheckIn(hotels[0]);
        person4.tryCheckIn(hotels[0]);
        hotels[0].seeGuests();
        person2.checkOut(hotels[0]);
        person4.tryCheckIn(hotels[0]);
        hotel1.seeGuests();
        hotels[1].seeGuests();


    }
}
