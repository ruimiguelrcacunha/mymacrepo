public class Person {

    private String name;
    private Key key = Key.FALSE;

    public Person(String name){
        this.name=name;

    }
    public void tryCheckIn(Hotel hotel){
        if (hotel == null){
            System.out.println("ERROR:\n In tryCheckIn, \"null\" is not accepted!");
            return;
        }
        key=hotel.checkIn(name, this);
        if(key == Key.DUPLICATED){
            key = Key.TRUE;
        }
    }
    public void checkOut(Hotel hotel){
        if (hotel == null){
            System.out.println("ERROR:\n In tryCheckOut, \"null\" is not accepted!");
            return;
        }
        hotel.checkOut(name,this);// o this chama a ele propio assim vai como se levase uma firgerprint de ele proprio
    }
}
