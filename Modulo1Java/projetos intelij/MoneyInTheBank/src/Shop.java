public class Shop {
    public static String input[] = new String[2];
    public static String[] shoes(double moneyToSpend){
        if (moneyToSpend<20){
            input[0]="10";
            input[1]="Nique - Pokemon Edition";
            return input;
        }
        else if(moneyToSpend>=20 && moneyToSpend<80){
            input[0]="20";
            input[1]="Nike Air";
            return input;
        }
        else if (moneyToSpend>=80 && moneyToSpend<200){
            input[0]="80";
            input[1]="Mr. shoe - Lagoste edition";
            return input;
        }
        else{
            input[0]="200";
            input[1]="Hugo boss - gold plated Shoes";
            return input;
        }
    }

    public static String[] pants(double moneyToSpend){
        if (moneyToSpend<20){
            input[0]="10";
            input[1]="Ardidas - Slav Style";
            return input;
        }
        else if(moneyToSpend>=20 && moneyToSpend<80){
            input[0]="20";
            input[1]="Nike CargoBob Pants";
            return input;
        }
        else if (moneyToSpend>=80 && moneyToSpend<200){
            input[0]="10";
            input[1]="Zara Man Silver Pants";
            return input;
        }
        else{
            input[0]="200";
            input[1]="Hugo boss - gold plated Trousers";
            return input;
        }
    }
    public static String[] watch(double moneyToSpend){
        if (moneyToSpend<20){
            input[0]="10";
            input[1]="REi-lex";
            return input;
        }
        else if(moneyToSpend>=20 && moneyToSpend<80){
            input[0]="20";
            input[1]="SWATCH";
            return input;
        }
        else if (moneyToSpend>=80 && moneyToSpend<200){
            input[0]="80";
            input[1]="CASIO CENAS";
            return input;
        }
        else{
            input[0]="200";
            input[1]="ROLEX - THE ORIGINAL";
            return input;
        }
    }
    public static String[] shirt(double moneyToSpend){
        if (moneyToSpend<20){
            input[0]="10";
            input[1]="T-shirt Fuma";
            return input;
        }
        else if(moneyToSpend>=20 && moneyToSpend<80){
            input[0]="20";
            input[1]="T-shirt Nike Air";
            return input;
        }
        else if (moneyToSpend>=80 && moneyToSpend<200){
            input[0]="80";
            input[1]="Luis vitelo Sweat";
            return input;
        }
        else{
            input[0]="200";
            input[0]="Gucci Sweat";
            return input;
        }
    }
}
