import java.util.Scanner;
public class Main {

    public static void main(String[] args){
        Person person1 = new Person("Zé Joaquim");
        Scanner scan = new Scanner(System.in);

        boolean exit=false;

        while(!exit){
            System.out.println("\nWhat do you wish to do ?\n1: Work\n2: Check account balance\n3: See Items\n4: Shopping\n5: Casino\n6: Exit");
            int in = scan.nextInt();
            switch (in){
                case 1:
                    System.out.println("How many hours do you wish to work?");
                    int hours = scan.nextInt();
                    person1.work(hours);
                    break;
                case 2:
                    person1.app();
                    break;
                case 3:
                    person1.showItems();
                    break;
                case 4:
                    System.out.println("What do you wish to shop for?\n1: Shoes\n2: Pants\n3: Watches\n4: Shirts");
                    int shop = scan.nextInt();
                    switch (shop){
                        case 1:
                            person1.shopping(ShopeType.SHOES,person1.account.getBalance());
                            break;
                        case 2:
                            person1.shopping(ShopeType.PANTS,person1.account.getBalance());
                            break;
                        case 3:
                            person1.shopping(ShopeType.WATCH,person1.account.getBalance());
                            break;
                        case 4:
                            person1.shopping(ShopeType.SHIRT,person1.account.getBalance());
                            break;
                    }
                    break;
                case 5:
                    person1.casino();
                    break;
                case 6:
                    exit=true;
                default:

            }
        }
    }
}
