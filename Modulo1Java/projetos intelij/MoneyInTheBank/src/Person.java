import java.util.Scanner;

public class Person {

    Bank account = new Bank();
    Scanner scan = new Scanner(System.in);

    private String[] items = new String[4];
    private String name;

    public Person(String name){
        this.name=name;
    }

    public String getName() {
        return name;
    }

    public void work(int hours){
        int maxWorkHours = 16;
        double hourlyWage = 5.2;
        if (hours>maxWorkHours){
            System.out.println("Can't work more than 16 hours!");
            return;
        }
        account.cashIn(hours*hourlyWage);
        System.out.println(name + " has worked " + hours + " hours and earned "+ hours*hourlyWage + " €.");
    }

    public void app(){
        System.out.println(name + ", you have " + account.getBalance() + "€.");
    }


    private void atm(double money){
        account.moneyOut(money);
    }


    public void shopping(ShopeType shopType, double moneyToSpend){
        double minMoney=10;
        if(moneyToSpend<minMoney){
            System.out.println("No money modeFocker!");
            return;
        }
        String[] out;
        switch (shopType) {
            case SHOES:
                out = Shop.shoes(moneyToSpend);
                atm(Double.parseDouble(out[0]));
                items[3] = out[1];
                System.out.println(name + " bought " + out[1] + " for " + out[0] + "€.");
                break;
            case PANTS:
                out = Shop.pants(moneyToSpend);
                atm(Double.parseDouble(out[0]));
                items[2] = out[1];
                System.out.println(name + " bought " + out[1] + " for " + out[0] + "€.");
            case WATCH:
                out = Shop.watch(moneyToSpend);
                atm(Double.parseDouble(out[0]));
                items[1] = out[1];
                System.out.println(name + " bought " + out[1] + " for " + out[0] + "€.");
            case SHIRT:
                out = Shop.shirt(moneyToSpend);
                atm(Double.parseDouble(out[0]));
                items[0] = out[1];
                System.out.println(name + " bought " + out[1] + " or " + out[0] + "€.");
        }
    }


    public void showItems(){
        boolean hasItems=false;
        for (String item : items) {
            if (item!=null){
                System.out.println(item);
                hasItems=true;
            }
        }
        if (!hasItems){
            System.out.println("You have no items!");
        }
    }


    public void casino(){
        System.out.println("How much do you want to bet?");
        int bet = scan.nextInt();
        if (bet > account.getBalance()){
            System.out.println("You don't have money! Get out!!");
        }
        else{
            atm(bet);
            account.cashIn(Casino.roulete(bet));
        }
    }
}
