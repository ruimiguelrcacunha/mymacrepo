public class Bank {

    private double balance=0;

    public void cashIn(double money){
        balance=balance+money;
    }

    public double getBalance() {
        return Math.round(balance);
    }

    public double moneyOut(double withdraw){
        if(withdraw>balance){
            System.out.println("you dont have that much money! You can only withdraw " + balance);
            return 0;
        }
        else{
            balance=balance-withdraw;
            return withdraw;
        }
    }
}
