import java.util.Scanner;

public class Casino {

    static Scanner scan = new Scanner(System.in);
    static int[] reds = {1,3,5,7,9,11,13,15,17,19,21,23,25,27,29};
    static int[] blacks = {2,4,6,8,10,12,14,16,18,20,22,24,26,28,30};
    static int betValue0 = 30;
    static int betValueNumbers = 10;
    static int betValueReds = 2;
    static int betValueBlacks = 2 ;

    public static double roulete (int bet){
        System.out.println("Where do you want to bet?\n1: Number\n2: Reds\n3: Blacks\nDefault: Exit");
        int betForm = scan.nextInt();
        int numberRolled=numberRoll();
        int betWin;
        switch (betForm){
            case 1:
                int betNumber=31;
                while(betNumber<0 || betNumber>30) {
                    System.out.println("Which number [0 - 30]?");
                    betNumber= scan.nextInt();
                }
                System.out.println("The ball stopped on " + numberRolled);
                if (betNumber==0){

                    if(numberRolled == 0){
                        betWin=betValue0*bet;
                        System.out.println("You won " + betWin + " €. Nice!");
                        return betWin;
                    }
                }
                else{
                    if(numberRolled == betNumber){
                        betWin = betValueNumbers * bet;
                        System.out.println("You won " + betWin + " €. Nice!");
                        return betWin;
                    }
                }
                break;
            case 2:
                System.out.println("The ball stopped on " + numberRolled);
                for (int i : reds){
                    if (i == numberRolled){
                        betWin = betValueReds * bet;
                        System.out.println("You won " + betWin + " €. Nice!");
                        return betWin;
                    }
                }
                break;
            case 3:
                System.out.println("The ball stopped on " + numberRolled);
                for (int i : blacks) {
                    if (i == numberRolled) {
                        betWin = betValueBlacks*bet;
                        System.out.println("You won " + betWin + " €. Nice!");
                        return betWin;
                    }
                }
                break;
            default:
                return 0;
        }
        System.out.println("You lost!");
    return 0;
    }
    private static int numberRoll(){
        return (int)(Math.random()*30);//random 0 to 30

    }
}
