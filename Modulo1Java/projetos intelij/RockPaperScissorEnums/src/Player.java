public class Player {

    private String name;
    public Player(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }

    private TypesOfHands hand;
    public void chooseHand(){
        hand = TypesOfHands.values()[(int) (Math.random() * TypesOfHands.values().length)];
    }
    public TypesOfHands getHand(){return hand;}

    private int score=0;
    public void incrementScore(){
        score++;
    }
    public int getScore() {
        return score;
    }
}
