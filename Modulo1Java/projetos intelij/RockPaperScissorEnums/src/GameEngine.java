public class GameEngine {


    int numberOfRounds;
    String name1;
    String name2;
    public GameEngine(int numberOfRounds, String name1, String name2){
        this.numberOfRounds=numberOfRounds;
        this.name1=name1;
        this.name2=name2;
    }


    private int round;
    public void start(){
        Player player1 = new Player(name1);
        Player player2 = new Player(name2);
        boolean gameOver=false;
        int round = 0;
        while (!gameOver){
            round++;
            player1.chooseHand();
            player2.chooseHand();
            String validation = validation(player1.getHand(),player2.getHand());
            System.out.println("Round: " + round);
            System.out.println(player1.getName() + ": " +player1.getHand().toString() + "\t" + player2.getName() + ": "+player2.getHand().toString());
            switch (validation) {
                case "player1":
                    player1.incrementScore();
                    System.out.println(player1.getName() + " wins");
                    break;
                case "player2":
                    player2.incrementScore();
                    System.out.println(player2.getName() + " wins");
                    break;
                case "tie":
                    System.out.println("It's a tie!");
            }
            System.out.println(player1.getName() + ": " + player1.getScore() + "\t"+ player2.getName() + ": " + player2.getScore() + "\n");
            if(player1.getScore()==(numberOfRounds/2+1)){
                System.out.println(player1.getName() + " wins the game!");
                gameOver=true;
            }
            else if (player2.getScore()==(numberOfRounds/2+1)){
                System.out.println(player2.getName() + " wins the game!");
                gameOver=true;
            }


        }

    }
    public String validation(TypesOfHands hand1, TypesOfHands hand2){
        if(hand1==TypesOfHands.ROCK){
            switch (hand2){
                case ROCK:
                    return "tie";
                case PAPER:
                    return "player2";
                case SCISSOR:
                    return "player1";
            }
        }
        else if(hand1==TypesOfHands.PAPER){
            switch (hand2){
                case ROCK:
                    return "player1";
                case PAPER:
                    return "tie";
                case SCISSOR:
                    return "player2";
            }
        }
        else if(hand1==TypesOfHands.SCISSOR){
            switch (hand2){
                case ROCK:
                    return "player2";
                case PAPER:
                    return "player1";
                case SCISSOR:
                    return "tie";
            }
        }
        return "shit";
    }


}
