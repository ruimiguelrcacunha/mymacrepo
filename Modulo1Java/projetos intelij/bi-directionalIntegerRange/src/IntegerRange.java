import java.util.Iterator;

public class IntegerRange implements Iterable<Integer>{




    private int min;
    private int max;
    private int counter;

    public boolean forward;

    public IntegerRange(int min, int max, boolean forward){
        this.min = min;
        this.max = max;
        this.forward = forward;

        if(forward){
            counter=min;
        }
        else {
            counter=max;
        }

    }



    @Override
    public Iterator<Integer> iterator() {

        Iterator<Integer> it = new Iterator<Integer>() {


                @Override
                public boolean hasNext () {
                if (forward) {
                    if (counter >= max) {
                        return true;
                    }
                    return false;
                } else {
                    if (counter <= max) {
                        return true;
                    }
                    return false;
                }
            }


                @Override
                public Integer next () {
                if (forward) {
                    return counter++;
                } else {
                    return counter--;
                }
            }

        };
        return it;
    }
}

