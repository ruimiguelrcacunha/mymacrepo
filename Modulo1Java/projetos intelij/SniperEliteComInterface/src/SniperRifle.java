import gameObjects.Barrel;
import gameObjects.Enemys.BodyParts;
import gameObjects.Shootable;


public class SniperRifle {


    public void shoot(Shootable obj){

        BodyParts bodyPart =  Randoms.bodyPartHit();
        if(obj instanceof Barrel){
            obj.hit(Randoms.chestshotDamage());
            return;
        }

        int shot;
        switch (bodyPart){

            case HEAD:
                shot = Randoms.headshotDamage();
                System.out.println("\nHeadShot!  Damage: " + shot );
                obj.hit(shot);
                return;

            case CHEST:
                shot = Randoms.chestshotDamage();
                System.out.println("\nChestShot!  Damage: " + shot);
                obj.hit(shot);
                return;

            case LEGS:
                shot = Randoms.legshotDamage();
                System.out.println("\nLegShot!  Damage: " + shot);
                obj.hit(shot);

            case MISS:
                //System.out.println("\nShot missed!");
                //if(enemy instanceof ArmouredEnemy){
                //    ((ArmouredEnemy) enemy).armorBar();
               // }
               // enemy.showHealthBar();

        }
    }
}
