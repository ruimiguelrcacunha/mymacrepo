import gameObjects.*;
import gameObjects.Enemys.ArmouredEnemy;
import gameObjects.Enemys.Enemy;


public class Game {
    //variables
    private int numberOfGameObjects;
    private GameObjects[] gameObjects;
    private SniperRifle weapon;
    private int shotsFired = 0;

    //class constructor
    public Game(){
        this.numberOfGameObjects = Randoms.numberOfGameObjects();
        this.gameObjects = new GameObjects[numberOfGameObjects];
        this.weapon = new SniperRifle();
    }

    public void start() {

        createObjects();

        for (GameObjects obj : gameObjects) {
            if (obj instanceof Tree) {
                System.out.println("\n" + obj.getMessage() + "\n");
                continue;
            }
            if(obj instanceof Barrel){
                System.out.println("\n***** " + obj.getMessage() + " *****");
                while(!(((Barrel) obj).isDestroyed())){
                weapon.shoot((Shootable) obj);
                }
               /*
                switch(((Barrel) obj).getBarrelType()){
                    case PLASTIC:
                        break;
                    case WOOD:
                        break;
                    case METAL:
                        break;
                }*/
            }

            if(obj instanceof Enemy) {
                System.out.println("\n***** " + obj.getMessage() + " *****");
                if (((Enemy) obj) instanceof ArmouredEnemy) {
                    ((ArmouredEnemy) obj).showArmorBar();
                }
                ((Enemy) obj).showHealthBar();


                while (!((Enemy) obj).isDead()) {

                    weapon.shoot((Shootable) obj);
                    shotsFired++;

                }
                System.out.println("DEAD!!");
                System.out.println("\nYou shot " + shotsFired + " times!\n********************");
                shotsFired = 0;
            }
        }
    }



    private void createObjects(){
        for(int i = 0 ; i<numberOfGameObjects; i++ ){
            gameObjects[i]=Randoms.objects();

        }
    }



}
