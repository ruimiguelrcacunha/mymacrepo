import gameObjects.Barrel;
import gameObjects.BarrelType;
import gameObjects.Enemys.ArmouredEnemy;
import gameObjects.Enemys.BodyParts;
import gameObjects.Enemys.SoldierEnemy;
import gameObjects.GameObjects;
import gameObjects.Tree;
public class Randoms {


    public static int numberOfGameObjects() {
        int maxObjects = 20;
        int minObjects = 10;
        return ((int) Math.random() * (maxObjects - minObjects) + minObjects);
    }


    public static GameObjects objects() {
        //30 % chance to be a tree and 70% chance of being an enemy

        int probObj = (int) (Math.random() * 100);
        if (probObj < 20) {
            return new Tree();
        } else if(probObj >= 30 && probObj<50) {
            BarrelType barrelType = BarrelType.values()[(int) (Math.random() * BarrelType.values().length)];
            switch (barrelType) {
                case PLASTIC:
                    return new Barrel(BarrelType.PLASTIC);
                case WOOD:
                    return new Barrel(BarrelType.WOOD);
                case METAL:
                    return new Barrel(BarrelType.METAL);
                default:
                    return null;
            }
        }else {
            // 20% chance of being armoured and 80% chance of being soldier

            int probEnemy = (int) (Math.random() * 100);
            if (probEnemy < 20) {
                return new ArmouredEnemy();
            }
            return new SoldierEnemy();
        }
    }


    public static BodyParts bodyPartHit() {
        // Prob Head = 10% && Chest = 65% && Legs = 25%
        int prob = (int) (Math.random() * 100);

        if (prob < 10) {
            return BodyParts.HEAD;
        } else if (prob >= 10 && prob < 35) {
            return BodyParts.LEGS;
        } else if (prob >= 35 && prob < 65) {
            return BodyParts.MISS;
        }
        return BodyParts.CHEST;
    }


    public static int headshotDamage() {
        int baseShot = 91;
        int critical = (int) (Math.random() * 100);
        if (critical < 10) {
            System.out.println("* Critical Hit *");
            return 220;
        }
        return (int) (Math.random() * (120 - baseShot) + baseShot);
    }


    public static int chestshotDamage() {
        int baseShot = 37;
        int maxShot = 69;
        int criticalShot = 90;
        int critical = (int) (Math.random() * 100);
        if (critical < 10) {
            System.out.println("* Critical Hit *");
            return (int) (Math.random() * ((criticalShot - maxShot) + maxShot));
        }
        return (int) (Math.random() * (maxShot - baseShot) + baseShot);
    }


    public static int legshotDamage(){
        int baseShot=20;
        int maxShot = 46;
        int criticalShot = 50;
        int critical = (int)(Math.random()*100);
        if(critical<10){
            System.out.println("* Critical Hit *");
            return (int)(Math.random()*((criticalShot-maxShot)+maxShot));
        }
        return (int)(Math.random()*(maxShot-baseShot)+baseShot);
    }


}
