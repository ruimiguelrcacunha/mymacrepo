package gameObjects;

public abstract class GameObjects {
    Objects type ;
    public GameObjects(Objects type){
        this.type=type;
    }

    public Objects getType() {
        return type;
    }

    public abstract String getMessage();
}
