package gameObjects;

public enum BarrelType {
    PLASTIC,
    WOOD,
    METAL;
}
