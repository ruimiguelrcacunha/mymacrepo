package gameObjects.Enemys;

public class ArmouredEnemy extends Enemy{


    public ArmouredEnemy(){
        super(EnemyType.ARMOURED);
    }


    private int armour = 100;


    public int armourHit(int shot) {
        this.armour = this.armour - shot;
        int armor2Health = Math.abs(armour);
        if(armour<=0){this.armour=0;}
        return armor2Health;
    }


    public void showArmorBar(){
        String armourBar="";
        for (int i = 0; i<armour; i+=5){
           armourBar = armourBar + "|";
        }
        System.out.println("AM: " + armourBar + " " + armour);
    }


    @Override
    public void hit(int shot){
        if(armour>0){
            int armor2Health = armourHit(shot);
            if(armour<=0){hit(armor2Health);}
            else{
                showArmorBar();
                showHealthBar();
               // System.out.println("Armor: " + armour + " || Health: " + getHealth() + "\n");
                return;}
        }
        else{
            healthDown(shot);
            showArmorBar();
            showHealthBar();
           // System.out.println("Armor: " + armour + " || Health: " + getHealth() + "\n");

            if(getHealth() <= 0){
                setDead();
            }
            return;
        }

    }
    @Override
    public String getMessage(){
        return "Armoured Soldier";
    }
}
