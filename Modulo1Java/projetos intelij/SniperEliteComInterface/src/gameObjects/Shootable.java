package gameObjects;

public interface Shootable {

    void hit(int shot);
}
