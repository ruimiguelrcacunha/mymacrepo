package gameObjects;



public class Barrel extends GameObjects implements Shootable {

    private final BarrelType barrelType;
    private int damage = 0;
    private boolean destroyed = false;
    public Barrel(BarrelType barrelType){
        super(Objects.BARREL);
        this.barrelType = barrelType;
    }

    public BarrelType getBarrelType() {
        return barrelType;
    }

    public boolean isDestroyed() {
        return destroyed;
    }

    public void destroyed2True() {
        this.destroyed = true;
    }

    @Override
    public String getMessage(){
        return "barrel!";
    }

    public void hit(int shot){
        if(isDestroyed()){return;}

        if(barrelType.equals(BarrelType.PLASTIC)){
            System.out.println("Plastic Barrel shot!! BAAAAMMM!!");
            destroyed2True();
            System.out.println("DESTROYED!!");
            return;
        }
        if(barrelType.equals(BarrelType.WOOD)){
            damage=damage+shot;
            System.out.println("Wood Barrel shot!! BAAAAMMM!!");
            if(damage>50){destroyed2True();
            System.out.println("DESTROYED!!");}
            return;
        }
        damage=damage+shot;
        System.out.println("Metal Barrel shot!! BAAAAMMM!!");
        if(damage>75){destroyed2True();
        System.out.println("DESTROYED!!");}

        return;

    }



}

