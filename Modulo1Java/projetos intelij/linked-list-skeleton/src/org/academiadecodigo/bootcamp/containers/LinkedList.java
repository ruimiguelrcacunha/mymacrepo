package org.academiadecodigo.bootcamp.containers;

public class LinkedList {

    private Node head;
    private int length = 0;

    public LinkedList() {
        this.head = new Node(null);
    }

    public int size() {
        return length;
    }

    /**
     * Adds an element to the end of the list
     * @param data the element to add
     */
    public void add(Object data)  {

        Node node = new Node(data);
        Node iterator = head;
        while (iterator.getNext() != null){
            iterator = iterator.getNext();
        }
        iterator.setNext(node);
        length++;

    }

    /**
     * Obtains an element by index
     * @param index the index of the element
     * @return the element
     */
    public Object get(int index) {
        Node node = null;

        if(size()==0){
            return null;
        }

        for (int i = 0; i<=index; i++ ){

            if(i==0){
                node = head.getNext();
                if(node == null){return null;}

            }
            else{
                node = node.getNext();
                if(node == null){return null;}
            }

        }

        return node.getData();

    }

    /**
     * Returns the index of the element in the list
     * @param data element to search for
     * @return the index of the element, or -1 if the list does not contain element
     */
    public int indexOf(Object data) {
        Node node = head.getNext();

        for (int i = 0; i < size(); i++){

            if(data.equals(node.getData())){return i;}
            node = node.getNext();

            /*
            if(i==0){
                node = head.getNext();
                if(data.equals(node.data)){return i;}
            }
            else{
                node = node.getNext();
                if(data.equals(node.data)){return i;}
            }

             */
        }
        return -1;
    }

    /**
     * Removes an element from the list
     * @param data the element to remove
     * @return true if element was removed
     */
    public boolean remove(Object data) {


        System.out.println("new " + size());

        if(size()==0 || size()==1){
            this.head.setNext(null);
            return false;
        }


        Node node = head.getNext();
        Node targetnode = null;

        int count=0;
        int i = 0;

        while(node.getNext() != null){
            if(node.getData().equals(data)){
                targetnode=node.getNext();
                break;
            }
            count++;
        }
        while (targetnode != null){
            if(i==count-1){
                node.setNext(targetnode);
                length--;
                return true;
            }
            i++;
        }

        return false;



/*
        int indexToRemove = indexOf(data);
        System.out.println("index: " + indexToRemove);



        if(size() == 0 || indexToRemove == -1) {
            return false;
        }
        if(size() == 1){
            System.out.println("entrou");
            this.head.setNext(null);
            length--;
            return true;
        }


        Node node = head.getNext();
        Node nextToTheOneRemoved = null;

        for (int i = 0; i<=indexToRemove; i++){
            if(indexToRemove==i){
                nextToTheOneRemoved = node.getNext();
            }
            node = node.getNext();
        }

        if(size()==1 || indexToRemove == 0){
            head.setNext(nextToTheOneRemoved);
            length--;
            return true;

        }


        for (int i = 0; i< indexToRemove; i++){
            if(indexToRemove-1 == i){
                if(i==0){
                    head.setNext(nextToTheOneRemoved);
                    length--;
                    return true;
                }

                node.setNext(nextToTheOneRemoved);
                length--;
                return true;
            }
            node = node.getNext();
        }

        return false;



*/


    }

    private class Node {

        private Object data;
        private Node next;

        public Node(Object data) {
            this.data = data;
            next = null;
        }

        public Object getData() {
            return data;
        }

        public void setData(Object data) {
            this.data = data;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }

}
