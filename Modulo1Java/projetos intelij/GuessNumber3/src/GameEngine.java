public class GameEngine{
    private int max;
    private int min;
    private int nPlayers;
    private Players[] players;

    private final int correctGuess =RandomGenerator.chooseNumber(max,min);

    public GameEngine (int min, int max, int nPlayers){
        this.max=max;
        this.min=min;
        this.nPlayers=nPlayers;
    }

    public void start() {
        Players[] players = new Players[nPlayers];
        for (int i = 0; i < nPlayers; i++) {
            players[i]=new Players(i);

        }
        boolean gameOver = false;
        while (gameOver == false) {
            for (int i = 0; i < nPlayers; i++) {
                gameOver = check(correctGuess, players[i].getNumber(min, max), i);
                if (gameOver==true){break;}
            }
        }
    }
    private boolean check(int correctGuess, int guess, int player){
        if (correctGuess==guess){
            System.out.println("Nice player "+ player + "! "+ guess + " is the correct number!");
            return true;
        }
        else{
            System.out.println("Player "+ player + " your guess "+ guess + " is wrong, try again!!");
            return false;
        }
    }

}
