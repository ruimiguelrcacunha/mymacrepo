import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {


        FileInputStream inputStream = new FileInputStream("/Users/codecadet/Desktop/Modulo1Java/projetos intelij/InputOutputStreams/src/ReadThatShit.txt");
        FileOutputStream outputStream = new FileOutputStream("NewFile.txt");

        double length = inputStream.getChannel().size(); // tamanho do input


        byte[] buffer = new byte[(int) length];
        int num = 0;


        num = inputStream.read(buffer);
        outputStream.write(buffer, 0, num);
        System.out.println(num);

        inputStream.close();
        outputStream.close();


    }

/*

    import java.io.FileInputStream;
    import java.io.FileOutputStream;
    import java.io.IOException;

    public class Main {
        public static void main(String[] args) throws IOException {
            FileInputStream inputStream = new FileInputStream("/Users/codecadet/Desktop/Modulo1Java/projetos intelij/InputOutputStreams/src/ReadThatShit.txt");
            FileOutputStream outputStream = new FileOutputStream("NewFile.txt");
            int num = 0;
            while (num != -1) {
                byte[] buffer = new byte[1024];
                num = inputStream.read(buffer);
                if (num != -1) {
                    outputStream.write(buffer, 0, num);
                }
            }
            inputStream.close();
            outputStream.close();
        }
    }

*/

}
