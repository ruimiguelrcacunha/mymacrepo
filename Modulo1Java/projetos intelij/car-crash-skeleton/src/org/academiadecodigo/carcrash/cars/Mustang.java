package org.academiadecodigo.carcrash.cars;
import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;


public class Mustang extends Car{



    private int speed=2;

   // private Movement

public Mustang(Position pos){
    super(CarType.MUSTANG,pos);
}

    @Override
    public Position getPos() {
        return pos;
    }



    @Override
    public void move(){

        MoveIt mov = MoveIt.values()[(int)(Math.random()*4)] ;

        switch (mov){
            case NORTH:
                if((getPos().getRow()-speed) < 0){move();}
                getPos().setRow(getPos().getRow()-speed);

                break;

            case ESTE:
                if((getPos().getCol()+speed) > Field.getWidth()){move();}
                getPos().setCol(getPos().getCol()+speed);
                break;

            case SOUTH:
                if((getPos().getRow()+speed) > Field.getHeight()){move();}
                getPos().setRow(getPos().getRow()+speed);
                break;

            case WEST:
                if((getPos().getCol()-speed) < 0){move();}
                getPos().setCol(getPos().getCol()-speed);
                break;
        }
    }


    @Override
    public String toString(){ // para aparecer as letras, se nao da os endereços de memoria
        return "M";
    }




}
