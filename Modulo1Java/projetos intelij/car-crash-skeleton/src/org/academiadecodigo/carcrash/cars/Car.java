package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

abstract  public class Car {




    public Car(CarType carType, Position pos){
        this.carType = carType;
        this.pos = pos;
    }

    /** The position of the car on the grid */

    public Position pos ;
    private CarType carType;
    private boolean crashed = false;
    private int speedRow;
    private int speedCol;

    public CarType getCarType() {
        return carType;
    }

    public Position getPos() {
        return pos;
    }

    public boolean isCrashed() {
        return crashed;
    }

    public void setCrashedTrue() {
        this.crashed = true;
    }



    public abstract void move();

}
