package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Field;
import org.academiadecodigo.carcrash.field.Position;

public class CarFactory {

    private static int carTypes = CarType.values().length; // length do enum



    public static  Car getNewCar() {


        switch(randomCar()){// creates a new car with a new position randomly generated
            case MUSTANG:
                return new Mustang(new Position(initialCol(),initialRow()));

            case FIAT:
                return new Fiat(new Position(initialCol(),initialRow()));


            default:
                return null;
        }

    }



    private static CarType randomCar(){
        return CarType.values()[(int)(Math.random()*carTypes)]; // Returns a random CarType
    }

    private static int initialCol(){
        return (int)(Math.random()*Field.getWidth());
    }
    private static int initialRow(){
        return (int)(Math.random()*Field.getHeight());
    }


}
