package org.academiadecodigo.carcrash.field;

public class Position {

    int col;
    int row;

    public Position(int col0, int row0){
        this.col = col0;
        this.row = row0;
    }

    public void setCol(int col) {
        this.col = col;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }
}
