package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;

public class Game {

    public static final int MANUFACTURED_CARS = 20;

    /** Container of Cars */
    private Car[] cars;

    /** Animation delay */
    private int delay;
    private boolean gameOver=false;



    public Game(int cols, int rows, int delay) {//max columns, max rows , time to w8 4 each position change


        Field.init(cols, rows);//initialization field
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {//works

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();
            //System.out.println("car type: " + cars[i].getCarType() + "\tcol: " + cars[i].getPos().getCol() + "\t row: " + cars[i].getPos().getRow());
        }

       Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (!gameOver) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();

            // Updates screen with cars from method above
            crashed(cars);
            Field.draw(cars);


            // verify if crashed
        }

    }

    private void moveAllCars() { // Updates car positions

        for (Car car : cars){

            if(car.isCrashed()){continue;}
            else{
                car.move();
            }

        }


    }

    private void crashed(Car[] cars){

        for (Car car1 : cars){
            for (Car car2 : cars){
                if(car1.equals(car2)){
                    continue;
                }
                else if((car1.getPos().getCol() == car2.getPos().getCol()) && car1.getPos().getRow() == car2.getPos().getRow()){
                    car1.setCrashedTrue();
                    car2.setCrashedTrue();
                }
                else{
                    continue;
                }
            }
        }
    }

}
