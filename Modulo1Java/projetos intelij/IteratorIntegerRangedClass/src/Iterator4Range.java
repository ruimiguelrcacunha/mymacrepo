import java.util.Iterator;

public class Iterator4Range implements Iterator<Integer>{

    int minCounter;
    int maxCounter;
    int counter;

    public Iterator4Range(int min, int max) {
        this.minCounter = min;
        this.maxCounter = max;
        this.counter = min;
    }



    @Override
    public boolean hasNext() {
        if(counter<=maxCounter){
            return true;
        }
        return false;
    }

    @Override
    public Integer next() {
        return counter++;

    }


}
