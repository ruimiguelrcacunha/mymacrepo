package org.academiadecodigo.carcrash.field;

import com.sun.corba.se.spi.ior.IdentifiableContainerBase;

public class Position {

    private int col;
    private int row;



    public Position(int col, int row){
        this.col = col;
        this.row = row;

    }



    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public void newColUp(int decrement){
        col = col - decrement;
    }
    public void newColDown(int increment){
        col = col + increment;
    }
    public void newRowRight(int increment){
        row = row + increment;
    }
    public void newRowLeft(int decrement){
        row = row - decrement;
    }
}
