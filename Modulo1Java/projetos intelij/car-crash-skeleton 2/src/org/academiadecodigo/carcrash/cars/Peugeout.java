package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.Game;
import org.academiadecodigo.carcrash.RandomShit;
import org.academiadecodigo.carcrash.field.Field;

public class Peugeout extends Car{

    private int speed=1;
    private Direction direction;

    public Peugeout(int col, int row){
        super(col,row);
        this.direction = RandomShit.randomUDRL();
    }

    @Override
    public String toString(){
        return "P";
    }



    public void movement() {
        if (direction.equals(Direction.UPRIGHT)) {
            if (getPos().getCol() - speed <= 0 || getPos().getRow() + speed >= Field.getWidth()) {
                directionDownLeft();
                movement();
            }
            else{
                for (int i=0; i<speed ; i++){
                    getPos().newColUp(1);
                    getPos().newRowRight(1);
                    Game.checkCrash();
                }
            }
        }
        else if (direction.equals(Direction.DOWNLEFT)){ // ta a dar coco
            if(getPos().getCol() + speed >= Field.getWidth() || getPos().getRow() - speed <= 0 ){
                directionUpRight();
                movement();
            }
            for (int i=0; i<speed ; i++){
                getPos().newColDown(1);
                getPos().newRowLeft(1);
                Game.checkCrash();
            }


        }else if(direction.equals(Direction.DOWNRIGHT)){
            if(getPos().getRow() + speed >= Field.getHeight() || getPos().getCol() + speed >= Field.getWidth()){
                directionUpLeft();
                movement();
            }
            for (int i=0; i<speed ; i++){
                getPos().newRowRight(1);
                getPos().newColDown(1);
                Game.checkCrash();
            }


        }
        else{
            if (getPos().getCol() - speed <= 0 || getPos().getRow() - speed  <= 0){
                directionDownRight();
                movement();
            }
            for (int i=0; i<speed ; i++){
                getPos().newRowLeft(1);
                getPos().newColUp(1);
                Game.checkCrash();
            }
        }
    }



    private void directionDownLeft(){
        direction = Direction.DOWNLEFT;
    }
    private void directionUpRight(){
        direction = Direction.UPRIGHT;
    }
    private void directionDownRight(){
        direction = Direction.DOWNRIGHT;
    }
    private void directionUpLeft(){
        direction = Direction.UPLEFT;
    }

    
    
}
