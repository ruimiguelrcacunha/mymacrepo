package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.field.Position;

abstract  public class Car {

    /** The position of the car on the grid */
    private Position pos;
    private int speed;
    private Direction direction;
    private boolean crashed  = false;


    public Car(int col, int row){
        this.pos = new Position(col, row);

    }

    public Position getPos() {
        return pos;
    }

    public boolean isCrashed() {
        return crashed ;
    }

    public void crashed(){
        this.crashed = true;
    }

    public abstract void movement();
}
