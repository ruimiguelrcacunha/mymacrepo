package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.Game;
import org.academiadecodigo.carcrash.RandomShit;
import org.academiadecodigo.carcrash.field.Field;

public class Mustang extends Car{


    private int speed=2;
    private Direction direction;

    public Mustang(int col, int row){
        super(col,row);
        this.direction = RandomShit.randomUpDown();
}

    @Override
    public String toString(){
        return "M";
    }



    public void movement() {
        if (direction.equals(Direction.UP)) {
            if (getPos().getCol() - speed <= 0) {
                directionDown();
                movement();
            }
            else{
                for (int i=0; i<speed ; i++){  // anda 1 casa de cada vez e ve se crashou com algum durante esse movimento, para nao saltar por cima de carros q ja la estavam
                    getPos().newColUp(1);
                    Game.checkCrash();
                }

            }
        }
        else{
            if(getPos().getCol() + speed >= Field.getWidth()){
                directionUp();
                movement();
            }
            else{
                for (int i=0; i<speed ; i++){
                    getPos().newColDown(1);
                    Game.checkCrash();
                }
            }
        }
    }



    private void directionUp(){
        direction = Direction.UP;
    }
    private void directionDown(){
        direction = Direction.DOWN;
    }


}


