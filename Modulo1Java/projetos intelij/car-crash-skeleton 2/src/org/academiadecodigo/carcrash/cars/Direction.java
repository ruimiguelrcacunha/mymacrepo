package org.academiadecodigo.carcrash.cars;

public enum Direction {
    UP,
    DOWN,
    RIGHT,
    LEFT,
    UPRIGHT,
    DOWNLEFT,
    UPLEFT,
    DOWNRIGHT;
}
