package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.RandomShit;
import org.academiadecodigo.carcrash.field.Field;

public class Delorean extends Car{


    private int speed=1;
    private Direction direction;

    public Delorean(int col, int row){
        super(col,row);
        this.direction = RandomShit.randomAll();
    }

    @Override
    public String toString(){
        return "D";
    }



    public void movement() {
        if (direction.equals(Direction.UP)) {
            if (getPos().getCol() - speed <= 0) {
                directionAll();
                movement();
            } else {
                getPos().newColUp(speed);
            }

        } else if (direction.equals(Direction.DOWN)) {
            if (getPos().getCol() + speed >= Field.getWidth()) {
                directionAll();
                movement();
            } else {
                getPos().newColDown(speed);
            }
        } else if (direction.equals(Direction.LEFT)) {
            if (getPos().getRow() - speed <= 0) {
                directionAll();
                movement();
            } else {
                getPos().newRowLeft(speed);
            }
        } else if (direction.equals(Direction.RIGHT)) {
            if (getPos().getRow() + speed >= Field.getHeight()) {
                directionAll();
                movement();
            } else {
                getPos().newRowRight(speed);

            }
        } else if (direction.equals(Direction.UPRIGHT)) {
            if (getPos().getCol() - speed <= 0 || getPos().getRow() >= Field.getWidth()) {
                directionAll();
                movement();
            } else {
                getPos().newColUp(speed);
                getPos().newRowRight(speed);
            }
        } else if (direction.equals(Direction.DOWNLEFT)) {
            if (getPos().getCol() + speed >= Field.getWidth() || getPos().getRow() <= 0) {
                directionAll();
                movement();
            } else {
                getPos().newColDown(speed);
                getPos().newRowLeft(speed);
            }
        }else if(direction.equals(Direction.DOWNRIGHT)){
            if(getPos().getRow() + speed >= Field.getHeight() || getPos().getCol() + speed >= Field.getWidth()){
                directionAll();
                movement();
            }
            getPos().newRowRight(speed);
            getPos().newColDown(speed);

        }
        else{
            if (getPos().getCol() - speed <= 0 || getPos().getRow() - speed  <= 0){
                directionAll();
                movement();
            }
            getPos().newRowLeft(speed);
            getPos().newColUp(speed);
        }
    }





    private void directionAll(){
        direction = RandomShit.randomAll();
    }


}
