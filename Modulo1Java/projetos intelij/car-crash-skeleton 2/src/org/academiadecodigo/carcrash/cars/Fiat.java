package org.academiadecodigo.carcrash.cars;

import org.academiadecodigo.carcrash.Game;
import org.academiadecodigo.carcrash.RandomShit;
import org.academiadecodigo.carcrash.field.Field;

public class Fiat extends Car{


    private int speed=1;
    private Direction direction;

    public Fiat(int col, int row){
        super(col,row);
        this.direction = RandomShit.randomRightLeft();
    }


    @Override
    public String toString(){
        return "F";
    }


    public void movement(){
        if (direction.equals(Direction.LEFT)){
            if(getPos().getRow() - speed  <= 0){
                directionRight();
                movement();
            }
            else{
                for (int i=0; i<speed ; i++){
                    getPos().newRowLeft(1);
                    Game.checkCrash();
                }

            }
        }
        else{
            if(getPos().getRow() + speed >= Field.getHeight()){
                directionLeft();
                movement();
            }
            else{
                for (int i=0; i<speed ; i++){
                    getPos().newRowRight(1);
                    Game.checkCrash();
                }


            }
        }

    }

    private void directionLeft(){
        direction = Direction.LEFT;
    }
    private void directionRight(){
        direction = Direction.RIGHT;
    }

}
