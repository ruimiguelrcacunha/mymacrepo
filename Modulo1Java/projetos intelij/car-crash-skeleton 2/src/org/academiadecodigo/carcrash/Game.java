package org.academiadecodigo.carcrash;

import org.academiadecodigo.carcrash.cars.Car;
import org.academiadecodigo.carcrash.cars.CarFactory;
import org.academiadecodigo.carcrash.field.Field;

public class Game {

    public static final int MANUFACTURED_CARS = 40;

    /** Container of Cars */
    private static Car[] cars;

    /** Animation delay */
    private int delay;

    public Game(int cols, int rows, int delay) {

        Field.init(cols, rows);
        this.delay = delay;

    }

    /**
     * Creates a bunch of cars and randomly puts them in the field
     */
    public void init() {

        cars = new Car[MANUFACTURED_CARS];
        for (int i = 0; i < cars.length; i++) {
            cars[i] = CarFactory.getNewCar();

        }

        Field.draw(cars);

    }

    /**
     * Starts the animation
     *
     * @throws InterruptedException
     */
    public void start() throws InterruptedException {

        while (true) {

            // Pause for a while
            Thread.sleep(delay);

            // Move all cars
            moveAllCars();
            checkCrash();
            // Update screen
            Field.draw(cars);

        }

    }

    private void moveAllCars() {
        for (Car car : cars){
            if(car.isCrashed()){continue;}
            car.movement();
        }
    }
    public static void checkCrash(){
        for (int i = 0; i< cars.length-1; i++){
            for (int j = i+1; j<cars.length; j++ ){
                if(cars[i].equals(cars[j])){continue;}
                if((cars[i].getPos().getCol() == cars[j].getPos().getCol()) && (cars[i].getPos().getRow() == cars[j].getPos().getRow())){
                    cars[i].crashed();
                    cars[j].crashed();
                }
            }
        }
    }



}
