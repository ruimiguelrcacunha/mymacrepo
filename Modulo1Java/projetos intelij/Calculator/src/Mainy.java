public class Mainy {
    public static void main (String[] args){
        Calculator nInspire = new Calculator("Texas","Grey");// inicialização de um novo objeto do tipo calculator.
        nInspire.brand = "Texas";
        nInspire.colour = "Grey";
        System.out.println("\tCalculator:\n\t\tBrand: " + nInspire.brand + "\n\t\tColour: " + nInspire.colour+ "\n");

        //Calculator t150Ti = new Calculator();// no caso de nao ter iniciador de metodo construtor na classe calculator usa se este, que é o default.
        //t150Ti.brand = "Texas";
        //t150Ti.colour = "blue and black";

        nInspire.display("add: "+nInspire.add(1,2));
        nInspire.display(nInspire.add(1.4,0.8));
        nInspire.display(nInspire.sub(1,2));
        nInspire.display(nInspire.mult(1,2));
        nInspire.display(nInspire.div(1,2));
        nInspire.display(nInspire.fact(5));
        nInspire.display(nInspire.expo(2,4));

        nInspire.display("Ceil: " + nInspire.roundIt(4.8,"ceil"));
        nInspire.display("Floor: " + nInspire.roundIt(4.8,"FlooR"));
        nInspire.display("Round: " + nInspire.roundIt(4.8,"rOUND"));

        double[] array = {3,5,15,-5,0};
        nInspire.display(nInspire.min(array));
        nInspire.display(nInspire.max(array));
    }
}