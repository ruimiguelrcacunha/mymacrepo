public class Calculator {
    public String brand;// Property creation
    public String colour = "Azul";//Property creation com default "Azul"
    public Calculator(String colour, String marca){ //criação de um metodo construtor onde já passo as propiedades (O NOME DO METODO É EM CAPS), vai substituir o default() -> (linha 3 mainy)
        this.colour=colour;// para ele saber q variavel ele esta a falar. se fosse colour=colour ele assumi tipo(5=5 ou seja a mesma variavel é igual a mesma variavel e nao sendo variaveis diferentes como é o caso.)
        brand=marca;
    }
    public  double add(double n1, double n2){
        return n1+n2;// da para ter 2 metodos com o mesmo nome desde que a sua assinatura sejam diferentes
    }               // ou seja eles tem o mesmo nome de variavel mas um recebe floats e da return a floats e outro a ints. As suas assinaturas sao diferentes
    public double add(int n1, int n2){
        return n1+n2;
    }
    public float sub(float n1, float n2){
        return n1-n2;
    }
    public float mult(float n1, float n2){
        return n1*n2;
    }
    public float div(float n1, float n2){
        return n1/n2;
    }
    public int fact(int n){
        if(n <= 1){return 1;}
        return n*fact(n - 1);
    }
    public float expo(float base,float exp){
        if (exp == 1){return base;}
        return base*expo(base,exp-1);
    }
    public void display (double n){
        System.out.println(n);
    }
    public void display (String n){
        System.out.println(n);
    }
    public double roundIt(double n,String type){
       switch (type.toUpperCase()){
           case "CEIL":
               return Math.ceil(n);
           case "FLOOR":
               return Math.floor(n);
           case "ROUND":
               return Math.round(n);
           default:
               System.out.println("Type not correct");
               return 0;
       }
    }
    public double min (double[] array){
        double minimum=array[0];
        for (int i=1 ; i<array.length; i++){
           if (array[i]<minimum){minimum=array[i];}
        }
        return minimum;
    }
    public double max (double[] array){
        double maximum=array[0];
        for (int i=1 ; i<array.length; i++){
            if (array[i]>maximum){maximum=array[i];}
        }
        return maximum;
    }
}