public class Room {

    int roomNumber;
    Person guest = null;
    boolean vacant = true;
    Hotel hotel;


    public Room(int roomNumber, Hotel hotel){
        this.roomNumber = roomNumber;
        this.hotel = hotel;

    }

    public Person getGuest() {
        return guest;
    }
    public void setGuest(Person guest) {
        this.guest = guest;
    }
    public void resetGuest() {
        this.guest = null;
    }

    public boolean isVacant() {
        return vacant;
    }
    public void setVacantTrue() {
        this.vacant = true;
    }
    public void setVacantFalse() {
        this.vacant = false;
    }

    public int getRoomNumber() {
        return roomNumber;
    }
}
