public class Person {

    String name;
    Room room = null;

    public Person(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void tryCheckIn(Hotel hotel){
        if (hotel==null){
            return;
        }
        room = hotel.checkIn(this);

    }
    public void tryCheckOut(Hotel hotel){
        if (hotel==null){
            return;
        }
        room = hotel.checkOut(this);
    }


}
