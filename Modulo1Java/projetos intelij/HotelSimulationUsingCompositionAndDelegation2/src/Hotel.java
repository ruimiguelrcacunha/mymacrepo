public class Hotel {

    private String hotelName;
    private int numberOfRooms;
    private Room[] rooms;

    public Hotel(String hotelName, int numberOfRooms){
        this.hotelName = hotelName;
        this.numberOfRooms = numberOfRooms;
        this.rooms = new Room[numberOfRooms];
        for (int i=0; i<numberOfRooms; i++){
            rooms[i] = new Room(i+1,this);
        }
    }

    public Room checkIn(Person guest){
        for(Room room : rooms) {
            if (room.getGuest() == null) {
                continue;
            }
            if (room.getGuest().equals(guest)) {
                System.out.println(guest.name + ", You are already check in!\n");
                return null;
            }
        }
        for(Room room : rooms) {
            if(room.isVacant() == true){
                room.setGuest(guest);
                System.out.println(guest.name + ", You are now checked in, in room number " + room.getRoomNumber());
                room.setVacantFalse();
                return room;
            }
        }
        System.out.println("Sorry " + guest.name + " , there are no rooms available!");
        return null;
    }

    public Room checkOut(Person guest){
        for (Room room : rooms){
            if (room.getGuest()==null){continue;}
            if(room.getGuest().equals(guest)){
                room.setVacantTrue();
                room.resetGuest();
                return null;
            }
        }
        System.out.println("You have no check in, can't check out!");
        return null;
    }
    public void guestInHotel(){
        int checkIfEmpty = 0;
        for (Room room : rooms){
            if (room.getGuest() != null){
                checkIfEmpty++;
                if(checkIfEmpty==1){
                    System.out.println("Guests in hotel: ");
                }
                System.out.println(room.getGuest().name);
            }
        }
        if (checkIfEmpty == 0){
            System.out.println("The hotel is empty");
        }
    }



}
