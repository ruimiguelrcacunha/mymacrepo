public class Main {
    public static void main(String [] args){

        Person person1 = new Person("Zé");

        Hotel[] hotels = {new Hotel("Hotel GiraDiscos",3), new Hotel("CenasHotel",2)};

        person1.tryCheckIn(hotels[0]);
        hotels[0].guestInHotel();

    }

}
