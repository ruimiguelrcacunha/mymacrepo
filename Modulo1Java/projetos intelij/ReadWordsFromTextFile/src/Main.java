import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        ReadAndSpit readAndSpit = new ReadAndSpit();
        int counter =0;

        while (readAndSpit.doIt()){

            System.out.println(++counter);

            for (String word:readAndSpit){
                System.out.println(word);
            }

            Thread.sleep(1000);

        }
    }
}

