import java.io.*;
import java.util.*;

public class ReadAndSpit implements Iterable<String> {

    private LinkedList<String> words = new LinkedList<>();

    FileReader in = new FileReader("/Users/codecadet/Desktop/Modulo1Java/projetos intelij/ReadWordsFromTextFile/src/TheHungerGames.txt");



    BufferedReader buffer = new BufferedReader(in);

    private String line = "";

    public ReadAndSpit() throws FileNotFoundException {
    }

    public boolean doIt() throws IOException {

        if (!words.isEmpty()) {
            words.clear();
        }

        if (((line = buffer.readLine()) != null)) {

            String[] wordArray = line.split(" ");

            for (String word : wordArray) {
                words.add(word);
            }
            return true;
        } else {
            buffer.close();
            return false;
        }
    }

    @Override
    public Iterator<String> iterator() {

        return words.iterator();
    }
}
