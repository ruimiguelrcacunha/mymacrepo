public class Player {
    private String name;
    private int score=0;
    private String hand;
    public Player(String name){
        this.name=name;
    }
    public int getScore(){
        return score;
    }
    public void incrementScore(){
        this.score++;
    }
    public String getName(){
        return this.name;
    }
    public String hand(){
        return hand=Randomizer.randomHand();
    }
    public String getHand(){
        return hand;
    }
}
