public class Randomizer {
    private static String[] typesOfHands={"Rock","Paper","Scissor"};
    public static String randomHand(){
        int rand = ((int)(Math.random()*3));
        return typesOfHands[rand];
    }
}
