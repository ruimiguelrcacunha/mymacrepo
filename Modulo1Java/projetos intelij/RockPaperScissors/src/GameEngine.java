public class GameEngine {
    private int numberOfRounds;
    public GameEngine(int numberOfRounds){
        this.numberOfRounds=numberOfRounds;
    }
    Player player1 = new Player("Jony");
    Player player2 = new Player("ZeTo");
    private boolean gameOver = false;
    public void start(){
        int round=0;
        while(!gameOver){
            String validation = validation(player1.hand(),player2.hand());
            System.out.println("\nRound: "+round+"\n"+player1.getName()+" choose "+player1.getHand()+" and "+ player2.getName()+" choose "+player2.getHand());
            if(validation.equals("player1")){
                System.out.println(player1.getName()+" won the round!");
                player1.incrementScore();
            }
            else if(validation.equals("player2")){
                System.out.println(player2.getName()+" won the round!");
                player2.incrementScore();
            }
            else {
                System.out.println("It's a tie! Nobody wins");
            }
            if (player1.getScore() == (Math.floor(numberOfRounds/2)+1) || player2.getScore() == (Math.floor(numberOfRounds/2)+1)){
                if(player1.getScore()>player2.getScore()){
                System.out.println("\nThe winner is "+ player1.getName()+"\n"+player1.getName()+": "+player1.getScore()+"\t"+player2.getName()+": "+player2.getScore());}
                else{
                    System.out.println("\nThe winner is "+ player2.getName()+"\n"+player1.getName()+": "+player1.getScore()+"\t"+player2.getName()+": "+player2.getScore());
                }
                gameOver=true;
            }
            round++;
        }
    }
    private String validation(String player1, String player2){
        if(player1.equals("Rock") && player2.equals("Paper")){return "player2"; }
        else if(player1.equals("Rock") && player2.equals("Scissor")){return "player1";}
        else if(player1.equals("Rock") && player2.equals("Rock")){return "tie";}
        else if(player1.equals("Paper") && player2.equals("Paper")){return "tie"; }
        else if(player1.equals("Paper") && player2.equals("Scissor")){return "player2";}
        else if(player1.equals("Paper") && player2.equals("Rock")){return "player1";}
        else if(player1.equals("Scissor") && player2.equals("Paper")){return "player1"; }
        else if(player1.equals("Scissor") && player2.equals("Scissor")){return "tie";}
        else if(player1.equals("Scissor") && player2.equals("Rock")){return "player2";}
        else{
            System.out.println("deu merda");
            return "merda";
        }
    }
}
