import java.util.Scanner;

public class Menu {

    public Menu(){
        this.fileManager = new FileManager();
    }

    static Scanner scan = new Scanner(System.in);
    FileManager fileManager;


    boolean exit = false;

    public void menu(){
        while(!exit) {
           switch (choice()){
               case 1:
                   fileManager.logIn();
                   break;

               case 2:
                   fileManager.logOut();
                   break;

               case 3:
                   try{
                       fileManager.createFile(fileName());
                   } catch (NotEnoughSpaceException e) {
                       System.out.println(e.getMessage());

                   } catch (NotEnoughPermissionsExecption e) {
                       System.out.println(e.getMessage());
                   }finally {
                       menu();
                   }

               case 4:
                   try{
                       fileManager.getFile(fileNumber()).getName();
                   }catch (NotEnoughPermissionsExecption e){
                       System.out.println(e.getMessage());
                       menu();
                   }
                   catch (FileNotFoundException e){
                       System.out.println(e.getMessage());
                       System.out.println("Try other file number.");
                       menu();
                   }
                   break;
               case 5:
                   exit = true;

           }



        }
    }

    private int choice(){
        System.out.println("******************\n* Menu:          *\n* 1) Login       *\n* 2) Logout      *\n* 3) Create File *\n* 4) Get file    *\n* 5) Exit        *\n******************");
        int answer = scan.nextInt();
        while(answer != 1 && answer != 2 && answer != 3 && answer != 4 && answer != 5 && answer != 6 ){
            System.out.println("Repeat!");
            answer = scan.nextInt();
        }
        return answer;
    }

    private String fileName(){
        System.out.println("Choose file name.");
        return scan.next();
    }

    private int fileNumber(){
        System.out.println("Insert file number.");
        return scan.nextInt();
    }

}
