
public class FileManager {


    public FileManager(){
        this.files = new File[2];
        this.fileCounter=0;
    }
//variables
    private Boolean loggedIn;
    private File[] files;
    private int fileCounter;

//getters and setters
    public void logIn(){
        loggedIn = true;
        System.out.println("You have Logged In.");
    }

    public void logOut(){
        loggedIn = false;
        System.out.println("You have Logged Out.");
    }

//methods
    public void createFile(String name) throws NotEnoughPermissionsExecption,NotEnoughSpaceException{

        if(!loggedIn){throw new NotEnoughPermissionsExecption("You have no permission, log in!");}

        if(files.length<fileCounter){
            throw new NotEnoughSpaceException("There is no more space in disk!");
        }
        files[fileCounter] =  new File(name);
        fileCounter++;
    }

    public File getFile(int i) throws NotEnoughPermissionsExecption,FileNotFoundException{

        if(!loggedIn){throw new NotEnoughPermissionsExecption("You have no permission, log in!");}


        if(files[i] == null){
                throw new FileNotFoundException("ERROR: File does NOT exist!");
            }
           return  files[i];
        }

    }




