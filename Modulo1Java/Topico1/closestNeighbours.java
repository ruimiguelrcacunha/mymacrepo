class closestNeighbours{
	public static void main(String[] args) {

	    int[] myArray = {0, 5, 1209, 6, 2, 111, 112, 33};
	    int[] result = findClosest(myArray);
	    System.out.println("The closest numbers are: "+result[0]+" and " +result[1]);
	}

	private static int[] findClosest(int[] numbers) {

	    int  diffmin = Math.abs(numbers[0]-numbers[1]);
	    int[] closest = {numbers[0],numbers[1]};

	    for (int i=1; i<numbers.length-1 ; i++){
	    	int diff = Math.abs(numbers[i]-numbers[i+1]);
		if (diffmin>diff){
			diffmin=diff;
			closest[0]=numbers[i];
			closest[1]=numbers[i+1];
		}
	    }
	    return closest;
	}
}
