// define a callback function to run when the dom is ready (windows.onLoad)
$(document).ready(function () {
    // ...
});


function successCallback(response) {
    initMap();
    populateTable(response)

}

function errorCallback(request, status, error) {
    // do something with the error
}

// perform an ajax http get request
$.ajax({
    url: 'http://javabank.herokuapp.com/api/customer',
    async: true,
    success: successCallback,
    error: errorCallback
});


function populateTable(customerData) {

    var table = "";

    for (var i in customerData) {
        table += "<tr>";
        table += "<td>"
            + customerData[i].id + "</td>"
            + "<td>" + customerData[i].firstName + "</td>"
            + "<td>" + customerData[i].lastName + "</td>"
            + "<td>" + customerData[i].email + "</td>"
            + "<td>" + customerData[i].phone + "</td>";
        table += "</tr>";
    }

    document.getElementById("result").innerHTML = table;
}

let map;

function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: {lat: 23.14561, lng: -82.35087},
        zoom: 11,
    });
}


// // fetch element by id
// var element = $('#id');

// // fetch elements by class name
// var elements = $('.className');

// // fetch elements by tag type
// var elements = $('p');

// // fetch elements where the id attribute contains str
// var elements = $('[id*=str]');

// var element = '<tr>'+ '<td>table entry</td>' + '</tr>';

// // create the new element and append it to every node with the given class name
// $(element).appendTo('.className');

// // specify a click event callback function for every button
// $('button').click(function(event){
//     ...
// });




