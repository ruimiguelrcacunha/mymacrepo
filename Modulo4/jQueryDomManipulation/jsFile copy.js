

/*
var customerData = [
    { "id": 1, "firstName": "Rui", "lastName": "Ferrão", "email": "rui@gmail.com", "phone": "777888" },
    { "id": 2, "firstName": "Sergio", "lastName": "Gouveia", "email": "sergio@gmail.com", "phone": "777999" },
    { "id": 3, "firstName": "Bruno", "lastName": "Ferreira", "email": "bruno@gmail.com", "phone": "777666" },
    { "id": 4, "firstName": "Rodolfo", "lastName": "Matos", "email": "rodolfo@gmail.com", "phone": "777333" }
];
*/


var ajax;

if (window.XMLHttpRequest) {
    // Mozilla, Safari, IE7+ ...
    ajax = new XMLHttpRequest();
} else if (window.ActiveXObject) {
    // IE 6 and older
    ajax = new ActiveXObject('Microsoft.XMLHTTP');
}

// run this when the ajax request completes
ajax.onreadystatechange = function () {
    if (ajax.readyState === 4 && ajax.status === 200) {
        // alert(ajax.responseText);

        console.log(ajax.responseText)
        jsonProcessing(ajax)


    }
};

// start the AJAX request
ajax.open('GET', 'http://javabank.herokuapp.com/api/customer', true);
ajax.setRequestHeader('Content-type', 'application/json');
ajax.send();


function populateTable(customerData) {

    var table = "";

    for (var i in customerData) {
        table += "<tr>";
        table += "<td>"
            + customerData[i].id + "</td>"
            + "<td>" + customerData[i].firstName + "</td>"
            + "<td>" + customerData[i].lastName + "</td>"
            + "<td>" + customerData[i].email + "</td>"
            + "<td>" + customerData[i].phone + "</td>";
        table += "</tr>";
    }

    document.getElementById("result").innerHTML = table;
}


function jsonProcessing(ajax) {

    populateTable(JSON.parse(ajax.responseText))
}

let map;
function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: 23.14561, lng: -82.35087},
    zoom: 11,
  });
}








