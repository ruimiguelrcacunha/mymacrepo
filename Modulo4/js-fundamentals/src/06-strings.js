/**
 * Reduce duplicate characters to a desired minimum
 */
exports.reduceString = function (str, amount) {

    let count = 0;
    let newStr = "";

    for (i = 0; i < str.length; i++) {

        if (str[i] !== str[i - 1]) {
            count = 0;

        }


        if ((str[i] === str[i + 1] && count < amount) || i===0) {
            newStr = newStr + str[i];
            count++;
            continue;
        }



    }
    return newStr;

};

/**
 * Wrap lines at a given number of columns without breaking words
 */
exports.wordWrap = function (str, cols) {




};

/**
 * Reverse a String
 */
exports.reverseString = function (str) {

    let newString = "";
    for (i = str.length - 1; i >= 0; i--) {

        newString = newString + str[i];
    }
    return newString;

};

/**
 * Check if String is a palindrome
 */
exports.palindrome = function (str) {



    for (i = 0; i < str.length; i++) {
        if (str[i] !== str[str.length - i - 1]) {
            return false;

        }
    }
    return true;

};
