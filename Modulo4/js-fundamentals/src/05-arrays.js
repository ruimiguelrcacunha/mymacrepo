/**
 * Determine the location of an item in the array
 */
exports.indexOf = function (arr, item) {

    return arr.indexOf(item);
};

/**
 * Sum the items of an array
 */
exports.sum = function (arr) {

    return arr.reduce((a, b) => a + b, + 0);
};

/**
 * Remove all instances of an item from an array
 */
exports.remove = function (arr, item) {

    return arr.filter(items => items != item);
};

/**
 * Remove all instances of an item from an array by mutating original array
 */
exports.removeWithoutCopy = function (arr, item) {

    
};

/**
 * Add an item to the end of the array
 */
exports.append = function (arr, item) {
    arr.push(item);
    return arr
};

/**
 * Remove the last item of an array
 */
exports.truncate = function (arr) {
    arr.pop();
    return arr;
};

/**
 * Add an item to the beginning of an array
 */
exports.prepend = function (arr, item) {
    arr.unshift(item);
    return arr;

};

/**
 * Remove the first item of an array
 */
exports.curtail = function (arr) {
    arr.shift();
    return arr;
};

/**
 * Join two arrays together
 */
exports.concat = function (arr1, arr2) {

    return arr1.concat(arr2);
};

/**
 * Add an item to an array in the specified position
 */
exports.insert = function (arr, item, index) {
    arr.splice(index, 0, item);
    return arr;
};

/**
 * Count the number of occurrences of an item in an array
 */
exports.count = function (arr, item) {

    count = 0;
    for (i = 0; i <= arr.length; i++) {

        if (arr[i] === item) {
            count++;
        }

    }

    return count;
};

/**
 * Find all items which container multiple occurrences in the array
 */
exports.duplicates = function (arr) {

    let newarr = [];
    arr.sort();
    let count = 0;
    for (i = 0; i < arr.length; i++) {


        if (arr[i] !== arr[i + 1]) {
            count = 0;
            continue;
        }

        if (arr[i] == arr[i + 1] && count > 0) {
            continue;
        }

        if (arr[i] === arr[i + 1]) {
            newarr.push(arr[i + 1]);
            count++;
        }


    }
    return newarr;


};

/**
 * Square each number in the array
 */
exports.square = function (arr) {
    return arr.map(item => Math.pow(item, 2));
};

/**
 * Find all occurrences of an item in an array
 */
exports.findAllOccurrences = function (arr, target) {

    let indexes = [];
  
    for (i = 0; i <= arr.length; i++) {

        if (arr[i] === target) {
            indexes.push(i)
            
        }

    }
    return indexes;

};
