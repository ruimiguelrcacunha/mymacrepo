/**
 * Convert a binary String to a Number
 */
exports.binaryToDecimal = function(str) {
    return parseInt(str, 2);
};

/**
 * Add two Numbers with a precision of 2
 */
exports.add =  function(a, b) {
    var a1 = parseFloat(a,10);
    var a2 = parseFloat(b,10);
    b1=a1+a2;
    return parseFloat(Number.parseFloat(b1).toPrecision(1),10)
};

/**
 * Multiply two Numbers with a precision of 4
 */
exports.multiply =  function(a, b) {

   c =  a * b

    return parseFloat(Number.parseFloat(c).toPrecision(2),10);

};
